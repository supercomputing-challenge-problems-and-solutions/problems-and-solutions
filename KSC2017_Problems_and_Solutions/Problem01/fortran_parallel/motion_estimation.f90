SUBROUTINE MotionEstimation( pImage, nWidth,nHeight,nSearchRange, nMV, &
                             nStaFrameNum,nEndFrameNum,nOffset)
USE PARAMS
IMPLICIT NONE
INTEGER::nWidth, nHeight,nsearchRange,nStaFrameNum,nEndFrameNum,nOffset
INTEGER::pImage(INT(WIDTH,8)*INT(HEIGHT,8)*INT(nStaFrameNum,8):INT(WIDTH,8)*INT(HEIGHT,8)*INT(nEndFrameNum,8))
INTEGER::nMV(nStaFrameNum:(2*nEndFrameNum-2)+1)
INTEGER::nProjRef(0:nWidth-1), pProjCur(0:nWidth-1)
INTEGER::I,J
INTEGER::nMatchNumx, nMatchNumY
INTEGER::LSHIFHT    ! function name

! by LKH
INTEGER::ST1,ST2, TMP
INTEGER(KIND=8)::ENDARRAY
INTEGER::END1,END2
INTEGER::LineMotionEstimation

nMatchNumX = nWidth - LSHIFT(nSearchRange,1)
nMatchNumY = nHeight - Lshift(nSearchRange,1)

ENDARRAY=INT(WIDTH,8)*INT(HEIGHT,8)*INT(nEndFrameNum,8)+1
TMP=nWidth*nHeight

DO I=nStaFrameNum,(nEndFrameNum-1)-1
   ST1=TMP*I;        END1=ENDARRAY-ST1
   ST2=TMP*(I+1);    END2=ENDARRAY-ST2
   PRINT '("Frame =",I5)',i
    CALL DataProjectionV_C(pImage(ST1:), nProjRef, nSearchRange, nMatchNumY, nWidth, nHeight,END1)
    CALL DataProjectionV_C(pImage(ST2:), pProjCur, nSearchRange, nMatchNumY, nWidth, nHeight,END2)
    nMV(2*i+0)=LineMotionEstimation(nProjRef, pProjCur, nSearchRange,nMatchNumX, &
                 nSearchRange,nWidth,nHeight)   
                 
    CALL DataProjectionH_C(pImage(ST1:), nProjRef, nSearchRange, nMatchNumX, nWidth, nHeight,END1)
    CALL DataProjectionH_C(pImage(ST2:), pProjCur, nSearchRange+nMV(2*i+0), nMatchNumX,  nWidth, nHeight,END2)

   nMV(2*i+1) = LineMotionEstimation(nProjRef, pProjCur, nSearchRange, &
               nMatchNumY, nSearchRange,nWidth, nHeight);
               
    CALL DataProjectionV_C(pImage(ST1:), nProjRef, nSearchRange, nMatchNumY, nWidth, nHeight,END1)
    CALL DataProjectionV_C(pImage(ST2:), pProjCur, nSearchRange+nMV(2*i+1),&
                            nMatchNumY, nWidth, nHeight,END2);
    nMV(2*i+0) = LineMotionEstimation(nProjRef, pProjCur, nSearchRange,&
                nMatchNumX, nSearchRange,nWidth, nHeight);  

END DO

END SUBROUTINE MotionEstimation


SUBROUTINE DataProjectionH_C(src, nProjectionY, nOffset, nProjectionNum, nWidth, nHeight,LENGTH)
IMPLICIT NONE
INTEGER::LENGTH
INTEGER::SRC(0:LENGTH)
INTEGER::nOffset, nProjectionNum, nWidth, nHeight
INTEGER::nProjectionY(0:nWidth-1)
INTEGER::I,J
nProjectionY(0:nHeight-1)=0
DO J=0,nHeight-1
    DO I=0,nProjectionNum-1
        nProjectionY(J)=nProjectionY(J)+SRC(nOffset+I+J*nWidth)
    END DO
END DO
END SUBROUTINE DataProjectionH_C
!=================================
SUBROUTINE DataProjectionV_C(src, nProjectionX, nOffset, nProjectionNum, nWidth, nHeight,LENGTH)
IMPLICIT NONE
INTEGER::LENGTH
INTEGER::SRC(0:LENGTH)
INTEGER::nOffset,nProjectionNum,nWidth,nHeight
INTEGER::nProjectionX(0:nWidth-1)
INTEGER::I,J

nProjectionX(0:nWidth-1)=0
DO J=0,nProjectionNum-1
    DO I=0,nWidth-1
        nProjectionX(I)=nProjectionX(I)+SRC(nOffset*nWidth+I+J*nWidth);
    END DO
END DO
END SUBROUTINE DataProjectionV_C

!================           
INTEGER FUNCTION SAD_C(ref, cur, nMV, nOffset, nMatchNum,nWidth,nHeight)
IMPLICIT NONE   
INTEGER::nWidth,nHeight
INTEGER::REF(0:nWidth-1), CUR(0:nHeight-1)
INTEGER::nMV, nOffset,nMatchNum
INTEGER::I 
INTEGER::ABS_VAL
SAD_C = 0
DO I=0,nMatchNum-1
    SAD_C = SAD_C + abs(REF(nOffset+nMV+I)-CUR(nOffset+I))
END DO

END FUNCTION SAD_C
!=============
INTEGER FUNCTION LineMotionEstimation(nRef, nCurr, nOffset, nMatchNum, nSearchRange,nWidth,nHeight) RESULT(nnMV)
IMPLICIT NONE
INTEGER::nWidth,nHeight
INTEGER::nRef(0:nWidth-1), nCurr(0:nHeight-1)
INTEGER::nOffset, nMatchNum,nSearchRange
INTEGER::LSHIFT
INTEGER::min_sad, sad, I
INTEGER::SAD_C

min_sad=LSHIFT(1,30)
nnMV=0
DO I=-nSearchRange,nSearchRange
    SAD=SAD_C(nRef,nCurr,i,nOffset, nMatchNum,nWidth,nHeight);
    IF(sad<min_sad)THEN
        nnMV=i
        min_sad=sad
    END IF
END DO
END FUNCTION LineMotionEstimation

