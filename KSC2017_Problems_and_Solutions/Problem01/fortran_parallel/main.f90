MODULE PARAMS
IMPLICIT NONE
INTEGER,PARAMETER::WIDTH = 6400
INTEGER,PARAMETER::HEIGHT = 3600
INTEGER,PARAMETER::SCAN= 320
INTEGER,PARAMETER::FRAMENUM=100
INTEGER,PARAMETER::SEEK_SET=0
END MODULE PARAMS
!============================================
PROGRAM MAIN
USE PARAMS
IMPLICIT NONE
INCLUDE 'mpif.h'
INTEGER::I,IERR
INTEGER,ALLOCATABLE::pImages(:)
INTEGER,ALLOCATABLE::nMV(:),nMV_sum(:)
CHARACTER(LEN=1)::TMP(0:WIDTH*HEIGHT-1)
INTEGER::CNT1, CNT2
REAL::t_sat, t_end
INTEGER::nprocs, myrank, ista,iend, isize,iendp;
INTEGER,ALLOCATABLE::pload(:), pdisp(:)

CALL MPI_Init(IERR)
CALL MPI_Comm_size(MPI_COMM_WORLD,nprocs,ierr)
CALL MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)

ALLOCATE(pload(0:nprocs-1))
ALLOCATE(pdisp(0:nprocs-1))

CALL para_range(0,FRAMENUM-2,nprocs,myrank,ista,iend,pload,pdisp)

isize = iend - ista + 1
WRITE(*,100)nprocs,myrank,ista,iend,isize,pload(myrank),pdisp(myrank)
100 FORMAT('nprocs = ',I2,', myrank = ',I2,', ista = ',I2,', iend = ',I2,', isize = ',I2,&
           ', pload = ',I2,', pdisp = ',I2)
ALLOCATE(pImages(0:INT(WIDTH,8)*INT(HEIGHT,8)*INT(isize+1,8)-1),STAT=IERR)
IF(IERR /=0) THEN
  PRINT*,'Memory alloc error 1'
  CALL MPI_FINALIZE(IERR)
  RETURN
END IF
ALLOCATE(nMV(0:(2*isize)-1),STAT=IERR)
IF(IERR /=0) THEN 
   IF(ALLOCATED(pImages)) DEALLOCATE(pImages)
   PRINT*,'Memory alloc error 2'
   CALL MPI_FINALIZE(IERR)
   RETURN
END IF
nMV=0
ALLOCATE(nMV_sum(0:(2*FRAMENUM-2)-1))
nMV_sum=0

CALL SYSTEM_CLOCK(CNT1)
OPEN(10,FILE='../video_6400_3600.yuv',FORM='UNFORMATTED',ACCESS="STREAM",STATUS='OLD',IOSTAT=IERR)
IF(IERR /=0) THEN
  PRINT*,'File open error'
  CALL MPI_FINALIZE(IERR)
  RETURN
END IF
DO I=0,ISIZE
  CALL FSEEK(10,INT((ista+i)*WIDTH,8)*INT(HEIGHT*3/2,8),SEEK_SET,IERR)
  READ(10)TMP(0:INT(WIDTH,8)*INT(HEIGHT,8)-1)
  pImages(INT(i*WIDTH,8)*INT(HEIGHT,8):INT((I+1)*WIDTH,8)*INT(HEIGHT,8)-1)=ICHAR(TMP(0:WIDTH*HEIGHT-1))
END DO
CLOSE(10)
CALL SYSTEM_CLOCK(CNT2)
PRINT*,'Read time =',(CNT2-CNT1),' msec'

CALL SYSTEM_CLOCK(CNT1)
CALL MotionEstimation(pImages, WIDTH, HEIGHT,SCAN,nMV,0, isize+1,ista)
CALL SYSTEM_CLOCK(CNT2)
PRINT*,'Motion time = ',(CNT2-CNT1),' msec'
nMV_sum=0
pload=2*pload
pdisp=2*pdisp

CALL MPI_Gatherv(nMV(0:),2*isize,MPI_INT,nMV_sum(0:),pload(0:),pdisp(0:),MPI_INT,0,MPI_COMM_WORLD,IERR)
IF(myrank==0)THEN
  OPEN(11,FILE='./result.txt',IOSTAT=IERR,STATUS='REPLACE')
  IF(IERR==0)THEN
    DO I=0,(FRAMENUM-2)
      WRITE(11,'(I5,1X,I5,1X,I5)')I,nMV_sum(2*i+0), nMV_sum(2*i+1)
    END DO
    CLOSE(11)
  END IF
ENDIF
!----------------
DEALLOCATE(pload,pdisp,nMV,nMV_sum)
IF(ALLOCATED(pImages)) DEALLOCATE(pImages)
CALL MPI_Finalize(ierr)
END PROGRAM MAIN
!============================================
SUBROUTINE para_range(n1,n2,nprocs,myrank,ista,iend, pload, pdisp)
IMPLICIT NONE
INTEGER::N1,N2,NPROCS,MYRANK,ISTA,IEND
INTEGER::PLOAD(0:NPROCS-1),PDISP(0:NPROCS-1)
INTEGER::IWORK1,IWORK2,I
iwork1 = (n2-n1+1)/nprocs
iwork2 = MOD((n2-n1+1),nprocs)
ista=myrank*iwork1 + n1 + MIN(myrank,iwork2)
iend=ista + iwork1 -1
IF(iwork2>myrank) iend = iend + 1
DO I=0,NPROCS-1
  pload(i)=iwork1
  IF(I<iwork2) pload(i)=pload(i)+1
END DO
pdisp(0)=0
DO I=1,NPROCS-1
  pdisp(i)=pdisp(i-1)+pload(i-1)
END DO
END SUBROUTINE para_range
