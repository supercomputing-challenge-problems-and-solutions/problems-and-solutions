PROGRAM Main

!use, intrinsic :: iso_c_binding

include 'mpif.h'

#define ALPHABET_LEN 256
#define NOT_FOUND pattern_length
#define MAX_STRING_LENGTH 256

integer :: rankID=0, nRanks=1
integer :: ierr
character(len=MAX_STRING_LENGTH) :: rankName
character(len=MAX_STRING_LENGTH) :: target_path
character(len=MAX_STRING_LENGTH) :: arg, argv(2), str
character,allocatable,dimension(:) :: target, pattern
integer :: i, argc
integer(8) :: target_length, pattern_length, search_idx, found_count
integer, allocatable, dimension(:) :: BCS, GSS
real(8) :: read_time, search_time

EXTERNAL read_targetfile

CALL HOSTNM(rankName)
CALL MPI_INIT(ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD,rankID,ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,nRanks,ierr)

argc = command_argument_count()

DO i=1, argc
   CALL GET_COMMAND_ARGUMENT(i, arg)
   argv(i) = arg
END DO

target_path=TRIM(argv(1))//CHAR(0)
CALL get_filesize(target_path,target_length)

if(target_length < 0) then
   print*,"Error: Cannot read target file ", target_path
   CALL MPI_Finalize(ierr)
   STOP
endif

if(rankID == 0) then
   print*,"--------------------------------------------------"
   print*,"- Read target: [ ", TRIM(target_path)," ]"

   ALLOCATE(target(0:target_length-1))

   read_time = 0.
   read_time = read_time - MPI_Wtime()

   CALL read_targetfile(target, target_length, target_path)

   read_time = read_time + MPI_Wtime()
   print*,"- Target length: ", target_length,"(read time: ",read_time," secs)"
   print*,"--------------------------------------------------"
endif

if(argc < 2) then
   print*, "Error: Cannot read pattern "
   deallocate(target)
   CALL MPI_Finalize(ierr)
   STOP
endif

pattern_length = len_trim(argv(2))
allocate(pattern(0:pattern_length-1))

str = argv(2)
do i=0,pattern_length-1
   pattern(i) = str(i+1:i+1)
enddo

if(rankID == 0) then
   print*, "- Pattern: [ ", pattern, " ]"
   print*, "- Pattern length: ", pattern_length
   print*, "--------------------------------------------------"
endif

allocate(BCS(0:ALPHABET_LEN-1))
allocate(GSS(0:pattern_length-1))

CALL make_BCS(BCS, pattern, pattern_length)
CALL make_GSS(GSS, pattern, pattern_length)

found_count = 0
search_time = 0.
if(rankID == 0) search_time = search_time - MPI_Wtime()

! DO NOT EDIT UPPER CODE //
!==============================================================================================================//

search_idx = 0
CALL do_search(target, size(target,kind=8), search_idx, target_length, pattern, pattern_length, BCS, GSS, found_count)

if(found_count < 0) then
   deallocate(target)
   deallocate(BCS)
   deallocate(GSS)
   deallocate(pattern)
   CALL MPI_Finalize(ierr)
endif

!==============================================================================================================//
! DO NOT EDIT LOWER CODE //
if(rankID == 0) then
   search_time = search_time + MPI_Wtime()
   print*,"- Found_count: ", found_count
   print*,"--------------------------------------------------"
   print*,"- Time: ", search_time, " secs"
   print*,"--------------------------------------------------"
endif

deallocate(target)
deallocate(BCS)
deallocate(GSS)
deallocate(pattern)

CALL MPI_Finalize(ierr)


CONTAINS

SUBROUTINE get_filesize(target_path,buffsize)
   character(*) :: target_path
   integer(8):: buffsize

   INQUIRE(FILE=target_path, SIZE=buffsize)

END SUBROUTINE get_filesize

END PROGRAM Main
