PROGRAM Main

!use, intrinsic :: iso_c_binding

include 'mpif.h'

#define ALPHABET_LEN 256
#define NOT_FOUND pattern_length
#define MAX_STRING_LENGTH 256

character(len=MAX_STRING_LENGTH) :: rankName
character(len=MAX_STRING_LENGTH) :: target_path
character(len=MAX_STRING_LENGTH) :: arg, str
character,allocatable,dimension(:) :: target, pattern
character(len=MAX_STRING_LENGTH),allocatable,dimension(:) :: argv
integer :: argc
integer(8) :: target_length, pattern_length, search_idx, found_count
integer, allocatable, dimension(:) :: BCS, GSS
real(8) :: read_time, search_time
integer i4

!For MPI
integer    :: ierr, rankID, nRanks
integer(8) :: mpi_found_count
integer(8) :: nChunksPerRank, nTotalChunks, overlap_length
integer(8) :: quotient, remainder
integer(8) :: i, chunkID, nFinishRana
integer(8) ::   chunk_found_count, call_count;
integer(8), allocatable, dimension(:) :: chunk_length, chunk_start_idx
character, allocatable, dimension(:) :: chunk
integer :: mpireq(2), mpitag
integer :: mpistat1(MPI_STATUS_SIZE), mpistat2(MPI_STATUS_SIZE)
integer :: request_rankID

EXTERNAL read_targetfile

CALL HOSTNM(rankName)

CALL MPI_INIT(ierr)
CALL MPI_COMM_RANK(MPI_COMM_WORLD,rankID,ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD,nRanks,ierr)

argc = command_argument_count()
allocate(argv(argc))

DO i4=1, argc
   CALL GET_COMMAND_ARGUMENT(i4, arg)
   argv(i4) = arg
END DO

target_path=TRIM(argv(1))//CHAR(0)
CALL get_filesize(target_path,target_length)

if(target_length < 0) then
   print*,"Error: Cannot read target file ", target_path
   CALL MPI_Finalize(ierr)
   STOP
endif

if(rankID == 0) then
   print*,"--------------------------------------------------"
   print*,"- Read target: [ ", TRIM(target_path)," ]"

   ALLOCATE(target(0:target_length-1))

   read_time = 0.
   read_time = read_time - MPI_Wtime()

   CALL read_targetfile(target, target_length, target_path)

   read_time = read_time + MPI_Wtime()
   print*,"- Target length: ", target_length,"(read time: ",read_time," secs)"
   print*,"--------------------------------------------------"
endif

if(argc < 2) then
   print*, "Error: Cannot read pattern "
   if(rankID ==0) deallocate(target)
   CALL MPI_Finalize(ierr)
   STOP
endif

pattern_length = len_trim(argv(2))
allocate(pattern(0:pattern_length-1))

str = argv(2)
do i4=0,pattern_length-1
   pattern(i4) = str(i4+1:i4+1)
enddo

if(rankID == 0) then
   print*, "- Pattern: [ ", pattern, " ]"
   print*, "- Pattern length: ", pattern_length
   print*, "--------------------------------------------------"
endif

allocate(BCS(0:ALPHABET_LEN-1))
allocate(GSS(0:pattern_length-1))

CALL make_BCS(BCS, pattern, pattern_length)
CALL make_GSS(GSS, pattern, pattern_length)

found_count = 0
search_time = 0.
if(rankID == 0) search_time = search_time - MPI_Wtime()

! DO NOT EDIT UPPER CODE //
!==============================================================================================================//

mpi_found_count=0

if(argc < 3) then
   print*, "Error: Check chunk size "
   if(rankID ==0) deallocate(target)
   deallocate(BCS)
   deallocate(GSS)
   CALL MPI_Finalize(ierr)
   STOP
endif

READ(argv(3),'(i8)') nChunksPerRank

if(nRanks==1) then
  print*, " # of Processes have to be more than 1"
   if(rankID ==0) deallocate(target)
   deallocate(BCS)
   deallocate(GSS)
  CALL MPI_Finalize(ierr)
  STOP
endif

nTotalChunks = (nRanks-1) * nChunksPerRank
overlap_length = (pattern_length - 1) * (nTotalChunks - 1)
quotient = (target_length + overlap_length) / nTotalChunks
remainder = (target_length + overlap_length) - (quotient * nTotalChunks)

chunkID = 0
ALLOCATE(chunk_length(0:nTotalChunks))
ALLOCATE(chunk_start_idx(0:nTotalChunks))

do i=0, nTotalChunks-1
   chunk_length(i) = quotient
enddo
do i=0, remainder-1
   chunk_length(i) = chunk_length(i) + 1
enddo

chunk_start_idx(0) = 0
do i=1, nTotalChunks-1
   chunk_start_idx(i) = chunk_start_idx(i-1) + chunk_length(i-1) - (pattern_length-1)
enddo

chunk_start_idx(nTotalChunks) = 0
chunk_length(nTotalChunks) = 0

mpitag=0
request_rankID = -1
if(rankID == 0) then
   nFinishRanks = 0
   do while(nFinishRanks < nRanks-1)
      CALL MPI_Recv(request_rankID, 1, MPI_INTEGER, MPI_ANY_SOURCE, mpitag, &
                    MPI_COMM_WORLD, mpistat1,ierr)

      CALL MPI_Isend(target(chunk_start_idx(chunkID)), chunk_length(chunkID), MPI_CHARACTER, &
                    request_rankID, chunkID, MPI_COMM_WORLD, mpireq(2),ierr)

      if(chunkID < nTotalChunks) then
         chunkID = chunkID + 1
      else
         nFinishRanks = nFinishRanks + 1
      endif
   end do
else
   ALLOCATE(chunk(0:chunk_length(0)-1))
   chunk_found_count = 0
   call_count = 0
   do while(chunkID < nTotalChunks)
      CALL MPI_Isend(rankID, 1, MPI_INTEGER, 0, mpitag, MPI_COMM_WORLD, mpireq(1), ierr)

      CALL MPI_Recv(chunk, chunk_length(0), MPI_CHARACTER, 0, MPI_ANY_TAG, &
                    MPI_COMM_WORLD, mpistat2, ierr)

      chunkID = mpistat2(MPI_TAG)

      if(chunkID < nTotalChunks) then
         CALL do_search(chunk, size(chunk,kind=8), int(0,kind=8), chunk_length(chunkID), &
                        pattern, pattern_length, BCS, GSS, chunk_found_count)
         if(chunk_found_count < 0) then
            deallocate(chunk)
            deallocate(BCS)
            deallocate(GSS)
            deallocate(chunk_length);
            deallocate(chunk_start_idx);
            CALL MPI_Finalize(ierr)
            STOP
          endif
          mpi_found_count = mpi_found_count + chunk_found_count
          call_count = call_count + 1
      endif
    enddo
    print*, "- [", rankID,":", TRIM(rankName),"] call_count: ", call_count
endif

CALL MPI_Reduce(mpi_found_count, found_count, 1, MPI_INTEGER8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

if(rankID /= 0) deallocate(chunk)
deallocate(chunk_length)
deallocate(chunk_start_idx)

!==============================================================================================================//
! DO NOT EDIT LOWER CODE //
if(rankID == 0) then
   search_time = search_time + MPI_Wtime()
   print*,"- Found_count: ", found_count
   print*,"--------------------------------------------------"
   print*,"- Time: ", search_time, " secs"
   print*,"--------------------------------------------------"
endif

if(rankID ==0) deallocate(target)
deallocate(BCS)
deallocate(GSS)
deallocate(pattern)

CALL MPI_Finalize(ierr)


CONTAINS

SUBROUTINE get_filesize(target_path,buffsize)
   character(*) :: target_path
   integer(8):: buffsize

   INQUIRE(FILE=target_path, SIZE=buffsize)

END SUBROUTINE get_filesize

END PROGRAM Main
