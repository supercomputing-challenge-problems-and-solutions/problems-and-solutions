#define ALPHABET_LEN 256
#define NOT_FOUND pattern_length

SUBROUTINE make_BCS(BCS, pattern, pattern_length)
   integer(8) :: pattern_length, i
   integer :: BCS(0:ALPHABET_LEN-1)
   character :: pattern(0:pattern_length-1)

   do i=0, ALPHABET_LEN-1
      BCS(i) = NOT_FOUND
   enddo
   do i=0, pattern_length-2
      BCS(IACHAR(pattern(i))) = pattern_length -1 - i
!      print*, pattern(i), IACHAR(pattern(i)), BCS(iachar(pattern(i)))
   enddo

END SUBROUTINE make_BCS

SUBROUTINE make_GSS(GSS, pattern, pattern_length)
   integer(8) :: pattern_length, i, last_prefix_index, suffix_length
   integer :: GSS(0:pattern_length-1)
   character :: pattern(0:pattern_length-1)
   logical :: is_prefix
   integer(8) :: get_suffix_length

   last_prefix_index = pattern_length-1
   do i=pattern_length-1, 0, -1
      if(is_prefix(pattern, pattern_length, i+1)) last_prefix_index = i + 1
      GSS(i) = last_prefix_index + (pattern_length -1 - i)
!      print*,i, GSS(i)
   enddo

   do i=0, pattern_length-2
      suffix_length = get_suffix_length(pattern, pattern_length, i)
      if(pattern(i - suffix_length) /= pattern(pattern_length-1 - suffix_length)) &
         GSS(pattern_length-1 - suffix_length) = pattern_length-1 - i + suffix_length
   enddo
!   print*, 'GSS=', GSS

END SUBROUTINE make_GSS

FUNCTION is_prefix(pattern, pattern_length, pattern_idx) result(TF)
   integer(8) :: i, pattern_length, pattern_idx, suffix_length
   character :: pattern(0:pattern_length-1)
   logical :: tf

   suffix_length = pattern_length - pattern_idx
   do i=0, suffix_length-1
      if(pattern(i) /= pattern(pattern_idx+i)) then
        tf = .false.
        RETURN
      endif
   enddo

   tf=.true.
END FUNCTION is_prefix

FUNCTION get_suffix_length(pattern, pattern_length, pattern_idx) RESULT(slength)
   integer(8) :: i, pattern_length, pattern_idx, slength
   character :: pattern(0:pattern_length-1)

   do i=0, pattern_idx-1
      if(pattern(pattern_idx-i) == pattern(pattern_length-1 - i)) then
        continue
      else
        exit
      endif
   enddo
   slength = i
!   print*, "pattern_idx,i", pattern_idx, slength

END FUNCTION get_suffix_length

SUBROUTINE do_search(target, target_length, search_idx, search_length, pattern, pattern_length, BCS, GSS, found_count)
   integer(8) :: ln, target_length, search_idx, pattern_length, search_length
   integer :: BCS(0:ALPHABET_LEN-1)
   integer :: GSS(0:pattern_length-1)
   character :: pattern(0:pattern_length-1)
   character :: target(0:target_length-1)
   integer(8) :: found_count !output

!local variables
   integer(8) :: target_cmp_idx, pattern_cmp_idx

   found_count=0

   if(pattern_length == 0) then
      print*,"Error: pattern_length = 0"
      STOP
   endif

   target_cmp_idx = pattern_length-1

   DO WHILE(target_cmp_idx < search_length)
      pattern_cmp_idx = pattern_length-1
      DO WHILE((pattern_cmp_idx >= 0) .AND. (target(search_idx + target_cmp_idx) == pattern(pattern_cmp_idx)))
         target_cmp_idx = target_cmp_idx -1
         pattern_cmp_idx = pattern_cmp_idx -1
      ENDDO

      if(pattern_cmp_idx < 0) then
         target_cmp_idx = target_cmp_idx + GSS(0)
         found_count = found_count+1
      else
         target_cmp_idx = target_cmp_idx + &
                   MAX(BCS(IACHAR(target(search_idx + target_cmp_idx))), GSS(pattern_cmp_idx))
      endif

      if(target_cmp_idx >= search_length) exit

      if(IACHAR(target(search_idx + target_cmp_idx)) < 0) then
         print*,"Error: target index error - ( ", target_length, " vs ", search_idx+target_cmp_idx
         STOP
      endif
   enddo

END SUBROUTINE do_search
