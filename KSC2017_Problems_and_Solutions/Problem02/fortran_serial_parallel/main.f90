program main

use blackhole_lab
use SBLspacetime

implicit none

integer,dimension(:),allocatable :: istatus
real(8),dimension(:),allocatable :: y, z

integer,parameter :: w_start = 0
integer,parameter :: w_end   = C_RESOLUTION_WIDTH  - 1
integer,parameter :: h_start = 0
integer,parameter :: h_end   = C_RESOLUTION_WIDTH  - 1
real              :: start, finish

allocate(istatus(0:C_RESOLUTION_TOTAL_PIXELS-1))
allocate(y(0:C_RESOLUTION_TOTAL_PIXELS-1))
allocate(z(0:C_RESOLUTION_TOTAL_PIXELS-1))

call cpu_time(start)

call run(w_start,w_end,h_start,h_end,istatus,y,z)
call export(istatus,y,z)

call cpu_time(finish)
write(*,'("Elapsed time: ",f10.6,"s")') finish-start

deallocate(istatus)
deallocate(y)
deallocate(z)

contains

subroutine ray_trace(w,h,istatus,y,z,idx)
  real(8)                                          :: w, h
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z
  integer                                          :: idx

  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1)                :: value, value_temp
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1,0:C_RK_ORDER-1) :: derivative=0.
  integer                                                     :: i, j, k, l

! set initial value
  do i=0,C_NUMBER_OF_FUNCTIONS-1
    value(init_order(i))=initial_value_function(init_order(i),w,h,value)
  enddo

! time integration
  do i=0,C_TOTAL_STEP-1
  if(bool_stop_condition(value)) exit
    do k=0,C_RK_ORDER
      do j=0,C_NUMBER_OF_FUNCTIONS-1
        value_temp(j)=value(j)
      enddo
      do l=0,k-1
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          value_temp(j)=value_temp(j)+RK_factor(l,k-1) * derivative(j,l) * C_DELTA_TIME
        enddo
      enddo
      if(C_RK_ORDER .eq. k) then
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          value(j)=value_temp(j)
        enddo
      else
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          derivative(j,k)=derivative_function(j,value_temp)
        enddo
      endif
    enddo
  enddo

! information return
  if(bool_cross_picture(value)) then
    istatus(idx)=HIT
    y(idx)=get_y(value)/C_l
    z(idx)=get_z(value)/C_l
  elseif(bool_near_horizon(value)) then
    istatus(idx)=FALL
  elseif(bool_outside_boundary(value)) then
    istatus(idx)=OUTSIDE
  else
    istatus(idx)=YET
  endif

end subroutine ray_trace

subroutine run(w_start,w_end,h_start,h_end,istatus,y,z)
  integer                                          :: w_start, w_end, h_start, h_end
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z

  integer :: h, w, k

  do h=h_start,h_end
    do w=w_start,w_end
      k=h * C_RESOLUTION_WIDTH + w
      call ray_trace(real(w,8),real(h,8),istatus,y,z,k)
    enddo
  enddo

end subroutine run

subroutine export(istatus,y,z)
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z

  integer,dimension(0:1) :: resolution

  resolution=[C_RESOLUTION_WIDTH, C_RESOLUTION_HEIGHT]

  open(UNIT=11, FILE=C_OUTPUT_FILENAME, STATUS="REPLACE", ACCESS="STREAM")

  write(11) resolution
  write(11) istatus
  write(11) y
  write(11) z

  close(UNIT=11)

end subroutine export

end program main
