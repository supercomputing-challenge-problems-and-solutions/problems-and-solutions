! Schwarzschild spacetime in Boyer-Lindquist coordinate
! All quantities are unitless by scaling with M, a mass of black hole, in the geometrized unit.

module SBLspacetime

implicit none

! Quantities
integer, parameter :: X_r=0, X_theta=1, X_phi=2, U_tt=3, U_r=4, U_theta=5, U_phi=6, C_NUMBER_OF_FUNCTIONS=7

contains

integer function I2(a,b)
  integer :: a,b

  I2=4*(a)+b

  return
end function I2

real(8) function g(i,var)
  integer              :: i
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8)              :: R,S,THETA

  R     = var(X_r)
  THETA = var(X_theta)
  S     = sin(THETA)

  select case(i)
    case(0)
      g=-(1.0_8-2.0_8/R)
    case(5)
      g=1./(1.0_8-2.0_8/R)
    case(10)
      g=R*R
    case(15)
      g=R*R*S*S
    case default
      g=0.
  end select

  return
end function

real(8) function gg(a,b,var)
  integer              :: a,b
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  gg=g(I2(a,b),var)

  return
end function gg

real(8) function U_unit(a,var)
  integer              :: a
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  U_unit=1.0_8/sqrt(gg(a,a,var))

  return
end function U_unit

! unit basis vectors
real(8) function U_r_unit(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  U_r_unit=U_unit(1,var)

  return
end function U_r_unit

real(8) function U_theta_unit(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  U_theta_unit=U_unit(2,var)

  return
end function U_theta_unit

real(8) function U_phi_unit(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  U_phi_unit=U_unit(3,var)

  return
end function U_phi_unit

real(8) function u_t(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  integer :: i

  u_t=0.
  do i=1,3
    u_t=u_t+gg(0,i,var)*var(U_tt+i)
  enddo

return
end function u_t

real(8) function u_square(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  integer :: i,j

  u_square=0.
  do i=1,3
    do j=1,3
      u_square=u_square+gg(i,j,var)*var(U_tt+i)*var(U_tt+i)
    enddo
  enddo

  return
end function u_square

! constraint
real(8) function U_t_constraint(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  U_t_constraint=-(u_t(var)+sqrt(u_t(var)*u_t(var)-gg(0,0,var)*u_square(var)))/gg(0,0,var)

  return
end function U_t_constraint

! Derivative functions
real(8) function derivative_X(a,var)
  integer              :: a
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_X=var(U_tt+a)/var(U_tt)

  return
end function derivative_X

real(8) function derivative_X_r(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_X_r=derivative_X(1,var)

  return
end function derivative_X_r

real(8) function derivative_X_theta(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_X_theta=derivative_X(2,var)

  return
end function derivative_X_theta

real(8) function derivative_X_phi(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_X_phi=derivative_X(3,var)

  return
end function derivative_X_phi

integer function I3(a,b,c)
  integer :: a,b,c

  I3=(16*a + 4*b + c)

  return
end function I3

real(8) function Gamma(i,var)
  integer              :: i
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: R, R2, R3, THETA, C, S, S2

  R=var(X_r)
  R2=R*R
  R3=R*R*R
  THETA=var(X_theta)
  C=cos(THETA)
  S=sin(THETA)
  S2=S*S

  select case(i)
    case(1,4)
      Gamma=1.0_8 / (R2 - 2.0_8*R)
    case(16)
      Gamma=(R - 2.0_8) / R3
    case(21)
      Gamma=1. / (2.0_8*R - R2)
    case(26)
      Gamma=2.0_8 - R
    case(31)
      Gamma=(2.0_8 - R)*S2
    case(38,41)
      Gamma=1.0_8 / R
    case(47)
      Gamma=-C*S
    case(55,61)
      Gamma=1.0_8 / R
    case(59,62)
      Gamma=C / S
    case default
      Gamma=0.0_8
  end select

  return
end function Gamma

real(8) function derivative_U(a,var)
  integer              :: a
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  integer :: i,j

  derivative_U=0.0_8
  do i=0,3
    do j=0,3
      derivative_U=derivative_U+Gamma(I3(a,i,j),var)*var(U_tt+i)*var(U_tt+j)
    enddo
  enddo
  derivative_U=-derivative_U/var(U_tt)

  return
end function derivative_U

real(8) function derivative_U_t(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_U_t=derivative_U(0,var)

  return
end function derivative_U_t

real(8) function derivative_U_r(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_U_r=derivative_U(1,var)

  return
end function derivative_U_r

real(8) function derivative_U_theta(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_U_theta=derivative_U(2,var)

  return
end function derivative_U_theta

real(8) function derivative_U_phi(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  derivative_U_phi=derivative_U(3,var)

  return
end function derivative_U_phi

real(8) function derivative_function(i,var)
  integer              :: i
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  select case(i)
    case(0)
      derivative_function=derivative_X_r(var)
    case(1)
      derivative_function=derivative_X_theta(var)
    case(2)
      derivative_function=derivative_X_phi(var)
    case(3)
      derivative_function=derivative_U_t(var)
    case(4)
      derivative_function=derivative_U_r(var)
    case(5)
      derivative_function=derivative_U_theta(var)
    case(6)
      derivative_function=derivative_U_phi(var)
  end select

  return
end function derivative_function

real(8) function get_x(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: R,S,PHI,THETA

  R=var(X_r)
  THETA=var(X_theta)
  PHI=var(X_phi)
  S=sin(THETA)

  get_x=R * S * cos(PHI)

  return
end function get_x

real(8) function get_y(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: R,S,PHI,THETA

  R=var(X_r)
  THETA=var(X_theta)
  PHI=var(X_phi)
  S=sin(THETA)

  get_y=R * S * sin(PHI)

  return
end function get_y

real(8) function get_z(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: R,C,THETA

  R=var(X_r)
  THETA=var(X_theta)
  C=cos(THETA)

  get_z=R * C

  return
end function get_z

logical function bool_near_horizon(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: R

  R=var(X_r)

  bool_near_horizon=(R<=3.0_8)

  return
end function bool_near_horizon

end module SBLspacetime
