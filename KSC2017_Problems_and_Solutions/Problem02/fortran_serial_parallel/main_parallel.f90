program main_parallel

use blackhole_lab
use SBLspacetime
use mpi

implicit none

integer,dimension(:),allocatable :: istatus, request, job_status
real(8),dimension(:),allocatable :: y, z
logical,dimension(:),allocatable :: performed

integer,parameter :: CHUNK_SIZE = 20
integer,parameter :: TAG_NEW_JOB = 0, TAG_JOB_COMPLETED = 1
integer,parameter :: STAT_READY = 0, STAT_START = 1, STAT_COMPLETED = 2

integer :: NUMBER_OF_JOBS_WIDTH
integer :: NUMBER_OF_JOBS_HEIGHT
integer :: NUMBER_OF_JOBS
integer :: w_start, w_end, h_start, h_end
integer :: my_rank, num_procs, ierr
integer :: i, rank, number_of_remaining_jobs, job_number, num_working_procs
real    :: start, finish

integer,dimension(MPI_STATUS_SIZE)    :: impi_status

NUMBER_OF_JOBS_WIDTH  = CEIL_DIV(C_RESOLUTION_WIDTH , CHUNK_SIZE)
NUMBER_OF_JOBS_HEIGHT = CEIL_DIV(C_RESOLUTION_HEIGHT, CHUNK_SIZE)
NUMBER_OF_JOBS        = NUMBER_OF_JOBS_WIDTH * NUMBER_OF_JOBS_HEIGHT

call MPI_INIT(ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank  , ierr)

!variables for masters
if((num_procs - 1) < NUMBER_OF_JOBS) then
  num_working_procs = num_procs - 1
else
  num_working_procs =  NUMBER_OF_JOBS
endif
allocate(request(0:num_working_procs))
allocate(job_status(0:NUMBER_OF_JOBS-1))

! variables for slaves
allocate(istatus(0:C_RESOLUTION_TOTAL_PIXELS-1))
allocate(y(0:C_RESOLUTION_TOTAL_PIXELS-1))
allocate(z(0:C_RESOLUTION_TOTAL_PIXELS-1))
allocate(performed(0:NUMBER_OF_JOBS-1))

performed=.false.

if(0 .eq. my_rank) then
  call cpu_time(start)
  do i=0,NUMBER_OF_JOBS-1
    job_status(i) = STAT_READY
  enddo

  ! isend new job messages to all slaves
  do i=0,num_working_procs-1
    rank = i+1
    call MPI_ISEND(i,1,MPI_INTEGER,rank,TAG_NEW_JOB,MPI_COMM_WORLD,request(rank),ierr)
    job_status(i) = STAT_START
  enddo
  number_of_remaining_jobs = NUMBER_OF_JOBS - num_working_procs

  do while(number_of_remaining_jobs > 0)
    ! receive a job completed message from any slave
    call MPI_RECV(job_number,1,MPI_INTEGER,MPI_ANY_SOURCE,TAG_JOB_COMPLETED,MPI_COMM_WORLD,impi_status,ierr)
    rank=impi_status(MPI_SOURCE)
    call MPI_REQUEST_FREE(request(rank),ierr)
    job_status(job_number)=STAT_COMPLETED

    ! isend a new job message to the above slave
    do i=0,NUMBER_OF_JOBS-1
      if(STAT_READY .eq. job_status(i)) then
        job_number = i
        exit
      endif
    enddo
    job_status(job_number)=STAT_START
    number_of_remaining_jobs=number_of_remaining_jobs-1
    call MPI_ISEND(job_number,1,MPI_INTEGER,rank,TAG_NEW_JOB,MPI_COMM_WORLD,request(rank),ierr)
  enddo

  do i=1,num_working_procs
    ! receive a last job completed message from all slaves
    call MPI_RECV(job_number,1,MPI_INTEGER,MPI_ANY_SOURCE,TAG_JOB_COMPLETED,MPI_COMM_WORLD,impi_status,ierr)
    rank=impi_status(MPI_SOURCE)
    call MPI_REQUEST_FREE(request(rank),ierr)
    job_status(job_number)=STAT_COMPLETED

    ! isend a no job message to the above slave
    job_number = -1
    call MPI_ISEND(job_number,1,MPI_INTEGER,i,TAG_NEW_JOB,MPI_COMM_WORLD,request(rank),ierr)
  enddo
else if(my_rank<=num_working_procs) then
  do while(.true.)
    ! receive a new job message from the master
    call MPI_RECV(job_number,1,MPI_INTEGER,0,TAG_NEW_JOB,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    if(job_number < 0) exit

    ! get a job domain from the job_number and run
    call get_job_domain(job_number,w_start,w_end,h_start,h_end)
    call run(w_start,w_end,h_start,h_end,istatus,y,z)
    performed(job_number) = .true.

    ! send a job completed message to the master
    call MPI_SEND(job_number,1,MPI_INT,0,TAG_JOB_COMPLETED,MPI_COMM_WORLD,ierr)
  enddo
endif

call MPI_BARRIER(MPI_COMM_WORLD,ierr)

if(0 .eq. my_rank) then
  do rank=1,num_working_procs
    call MPI_REQUEST_FREE(request(rank),ierr)
  enddo
endif
call export(my_rank,performed,istatus,y,z)

call MPI_FINALIZE(ierr)

if(0 .eq. my_rank) then
  call cpu_time(finish)
  write(*,'("Elapsed time: ",f10.6,"s")') finish-start
endif

deallocate(istatus)
deallocate(y)
deallocate(z)
deallocate(performed)
deallocate(request)
deallocate(job_status)

contains

integer function CEIL_DIV(x,y)
  integer :: x, y

  CEIL_DIV=((x) + (y) - 1) / (y)

  return
end function CEIL_DIV

subroutine get_job_domain(job_number,w_start,w_end,h_start,h_end)
  integer :: job_number, w_start, w_end, h_start, h_end

  integer :: i, j

  i = job_number / NUMBER_OF_JOBS_WIDTH
  j = mod(job_number, NUMBER_OF_JOBS_WIDTH)

  w_start = CHUNK_SIZE * j
  if(j .eq. NUMBER_OF_JOBS_WIDTH - 1) then
    w_end = C_RESOLUTION_WIDTH - 1
  else
    w_end = CHUNK_SIZE * (j + 1) - 1
  endif
  h_start = CHUNK_SIZE * i
  if(i .eq. NUMBER_OF_JOBS_HEIGHT - 1) then
    h_end = C_RESOLUTION_HEIGHT - 1
  else
    h_end = CHUNK_SIZE * (i + 1) - 1
  endif

  return
end subroutine

subroutine ray_trace(w,h,istatus,y,z,idx)
  real(8)                                          :: w, h
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z
  integer                                          :: idx

  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1)                :: value, value_temp
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1,0:C_RK_ORDER-1) :: derivative=0.
  integer                                                     :: i, j, k, l

! set initial value
  do i=0,C_NUMBER_OF_FUNCTIONS-1
    value(init_order(i))=initial_value_function(init_order(i),w,h,value)
  enddo

! time integration
  do i=0,C_TOTAL_STEP-1
  if(bool_stop_condition(value)) exit
    do k=0,C_RK_ORDER
      do j=0,C_NUMBER_OF_FUNCTIONS-1
        value_temp(j)=value(j)
      enddo
      do l=0,k-1
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          value_temp(j)=value_temp(j)+RK_factor(l,k-1) * derivative(j,l) * C_DELTA_TIME
        enddo
      enddo
      if(C_RK_ORDER .eq. k) then
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          value(j)=value_temp(j)
        enddo
      else
        do j=0,C_NUMBER_OF_FUNCTIONS-1
          derivative(j,k)=derivative_function(j,value_temp)
        enddo
      endif
    enddo
  enddo

! information return
  if(bool_cross_picture(value)) then
    istatus(idx)=HIT
    y(idx)=get_y(value)/C_l
    z(idx)=get_z(value)/C_l
  elseif(bool_near_horizon(value)) then
    istatus(idx)=FALL
  elseif(bool_outside_boundary(value)) then
    istatus(idx)=OUTSIDE
  else
    istatus(idx)=YET
  endif

end subroutine ray_trace

subroutine run(w_start,w_end,h_start,h_end,istatus,y,z)
  integer                                          :: w_start, w_end, h_start, h_end
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z

  integer :: h, w, k

  do h=h_start,h_end
    do w=w_start,w_end
      k=h * C_RESOLUTION_WIDTH + w
      call ray_trace(real(w,8),real(h,8),istatus,y,z,k)
    enddo
  enddo

end subroutine run

subroutine export(my_rank,performed,istatus,y,z)
  integer                                          :: my_rank
  integer,dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: istatus
  real(8),dimension(0:C_RESOLUTION_TOTAL_PIXELS-1) :: y, z
  logical,dimension(0:NUMBER_OF_JOBS-1)            :: performed

  integer                       :: fh, i, h, idx
  integer                       :: w_start, w_end, h_start, h_end
  integer(KIND=MPI_OFFSET_KIND) :: offset
  integer,dimension(0:1)        :: resolution

  resolution=[C_RESOLUTION_WIDTH, C_RESOLUTION_HEIGHT]

  offset = kind(0)*2 + (kind(0) + 2*kind(0_8))*C_RESOLUTION_TOTAL_PIXELS

  call MPI_FILE_OPEN(MPI_COMM_WORLD,C_OUTPUT_FILENAME,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),MPI_INFO_NULL,fh,ierr)
  call MPI_FILE_SET_SIZE(fh,offset,ierr)

  if(0 .eq. my_rank) then
    offset=0
    call MPI_FILE_WRITE_AT(fh,offset,resolution,2,MPI_INT,MPI_STATUS_IGNORE,ierr)
  else
    do i=0,NUMBER_OF_JOBS-1
      if(performed(i)) then
        call get_job_domain(i,w_start,w_end,h_start,h_end)
        do h=h_start,h_end
          idx = h * C_RESOLUTION_WIDTH + w_start
          offset = kind(0) * 2
          call MPI_FILE_WRITE_AT(fh,offset + kind(0) * idx, istatus(idx:), w_end - w_start + 1,MPI_INTEGER,MPI_STATUS_IGNORE,ierr)
          offset = offset + kind(0) * C_RESOLUTION_TOTAL_PIXELS
          call MPI_FILE_WRITE_AT(fh,offset + kind(0_8) * idx,       y(idx:), w_end - w_start + 1,MPI_REAL8,MPI_STATUS_IGNORE,ierr)
          offset = offset + kind(0_8) * C_RESOLUTION_TOTAL_PIXELS
          call MPI_FILE_WRITE_AT(fh,offset + kind(0_8) * idx,       z(idx:), w_end - w_start + 1,MPI_REAL8,MPI_STATUS_IGNORE,ierr)
        enddo
      endif
    enddo
  endif

  call MPI_FILE_CLOSE(fh,ierr)

end subroutine export

end program main_parallel
