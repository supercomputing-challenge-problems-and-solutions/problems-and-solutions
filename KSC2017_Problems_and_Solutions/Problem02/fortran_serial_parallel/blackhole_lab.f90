module blackhole_lab

use SBLspacetime

implicit none

! Parameters for lab setting
real(8), parameter :: C_PI           = acos(-1.0_8)
real(8), parameter :: C_L1           = 10.0_8 
real(8), parameter :: C_L2           = 10.0_8
real(8), parameter :: C_l            = 30.0_8
real(8), parameter :: C_VISUAL_ANGLE = 120.0_8
real(8), parameter :: C_d            = (1.0_8 / (sqrt(2.0_8) * tan(C_VISUAL_ANGLE / 2.0_8 / 180.0_8 * C_PI)))

! Parameters for resolution of vision
character*(*), parameter :: C_OUTPUT_FILENAME         = "data"
integer, parameter       :: C_RESOLUTION_WIDTH        = 100
integer, parameter       :: C_RESOLUTION_HEIGHT       = 100
integer, parameter       :: C_RESOLUTION_TOTAL_PIXELS = (C_RESOLUTION_WIDTH * C_RESOLUTION_HEIGHT)

! Parameters for time integration
real(8), parameter :: C_DELTA_TIME = -0.05_8
integer, parameter :: C_TOTAL_STEP = 20000
integer, parameter :: C_RK_ORDER   = 4

! For classification of rays
integer, parameter :: HIT     = 0
integer, parameter :: FALL    = 1
integer, parameter :: OUTSIDE = 2
integer, parameter :: YET     = 4

! For initialization of quantities
integer, parameter, dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: init_order = [X_r, X_theta, X_phi, U_r, U_theta, U_phi, U_tt]

real(8), parameter, dimension(0:0) :: RK_factor1 = [1.0_8 / 2.0_8                                             ]
real(8), parameter, dimension(0:1) :: RK_factor2 = [     0.0_8, 1.0_8 / 2.0_8                                 ]
real(8), parameter, dimension(0:2) :: RK_factor3 = [     0.0_8,      0.0_8,      1.0_8                        ]
real(8), parameter, dimension(0:3) :: RK_factor4 = [1.0_8 / 6.0_8, 1.0_8 / 3.0_8, 1.0_8 / 3.0_8, 1.0_8 / 6.0_8]
real(8), parameter, dimension(0:3,0:3) :: RK_factor = reshape((/(1.0_8/2.0_8),0.0_8        ,0.0_8        ,0.0_8        , &
                                                                0.0_8        ,(1.0_8/2.0_8),0.0_8        ,0.0_8        , &
                                                                0.0_8        ,0.0_8        ,1.0_8        ,0.0_8        , &
                                                                (1.0_8/6.0_8),(1.0_8/3.0_8),(1.0_8/3.0_8),(1.0_8/6.0_8)/)&
                                                               ,shape(RK_factor))

contains

logical function bool_outside_boundary(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: X, Y, Z

  X=get_x(var)
  Y=get_y(var)
  Z=get_z(var)

  bool_outside_boundary=(X > C_L1 .or. X < -C_L2 .or. abs(Y) > C_l .or. abs(Z) > C_l)

  return
end function bool_outside_boundary

logical function bool_cross_picture(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  real(8) :: X, Y, Z

  X=get_x(var)
  Y=get_y(var)
  Z=get_z(var)

  bool_cross_picture=(X < -C_L2 .and. abs(Y) < C_l/2. .and. abs(Z) < C_l/2.0_8)

  return
end function bool_cross_picture

logical function bool_stop_condition(var)
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  bool_stop_condition=(bool_outside_boundary(var) .or. bool_cross_picture(var) .or. bool_near_horizon(var))

  return
end function bool_stop_condition

real(8) function initial_value_X_r(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_X_r=C_L1

  return
end function initial_value_X_r

real(8) function initial_value_X_theta(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_X_theta=C_PI/2.0_8

  return
end function initial_value_X_theta

real(8) function initial_value_X_phi(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_X_phi=0.0_8

  return
end function initial_value_X_phi

real(8) function initial_value_U_r(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_U_r=C_d * U_r_unit(var)

  return
end function initial_value_U_r

real(8) function initial_value_U_theta(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_U_theta=-(-0.5 + h / real((C_RESOLUTION_HEIGHT - 1),8)) * U_theta_unit(var)

  return
end function initial_value_U_theta

real(8) function initial_value_U_phi(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_U_phi=-(-0.5_8 + w / real((C_RESOLUTION_WIDTH  - 1),8)) * U_phi_unit(var)

  return
end function initial_value_U_phi

real(8) function initial_value_U_t(w,h,var)
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  initial_value_U_t=U_t_constraint(var)

  return
end function initial_value_U_t

real(8) function initial_value_function(i,w,h,var)
  integer              :: i
  real(8)              :: w, h
  real(8),dimension(0:C_NUMBER_OF_FUNCTIONS-1) :: var

  select case(i)
    case(0)
      initial_value_function=initial_value_X_r(w,h,var)
    case(1)
      initial_value_function=initial_value_X_theta(w,h,var)
    case(2)
      initial_value_function=initial_value_X_phi(w,h,var)
    case(3)
      initial_value_function=initial_value_U_t(w,h,var)
    case(4)
      initial_value_function=initial_value_U_r(w,h,var)
    case(5)
      initial_value_function=initial_value_U_theta(w,h,var)
    case(6)
      initial_value_function=initial_value_U_phi(w,h,var)
  end select

  return
end function initial_value_function

end module blackhole_lab
