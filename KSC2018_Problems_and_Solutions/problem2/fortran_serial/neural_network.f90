module neural_network
   use mnist_file
   implicit none 

   type neural_network_t
		real :: b(0:MNIST_LABELS-1)
		real :: W(0:MNIST_LABELS-1, 0:MNIST_IMAGE_SIZE-1)
   end type neural_network_t

   type neural_network_gradient_t
		real :: b_grad(0:MNIST_LABELS-1)
		real :: W_grad(0:MNIST_LABELS-1, 0:MNIST_IMAGE_SIZE-1)
   end type neural_network_gradient_t

end module
