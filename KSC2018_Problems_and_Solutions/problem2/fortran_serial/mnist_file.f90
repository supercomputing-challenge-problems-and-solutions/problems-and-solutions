module mnist_file

   implicit none 
   integer(kind=4), parameter :: MNIST_LABEL_MAGIC = Z'00000801'
   integer(kind=4), parameter :: MNIST_IMAGE_MAGIC = Z'00000803'
   integer(kind=4), parameter :: MNIST_IMAGE_WIDTH = 28
   integer(kind=4), parameter :: MNIST_IMAGE_HEIGHT = 28
   integer(kind=4), parameter :: MNIST_IMAGE_SIZE = 784
   integer(kind=4), parameter :: MNIST_LABELS = 10

   type mnist_image_t
        SEQUENCE
        character :: pixels(0:MNIST_IMAGE_SIZE-1)
   end type mnist_image_t

   type mnist_dataset_t
       type (mnist_image_t), allocatable :: images(:)
       character, allocatable :: labels(:)
       integer(kind=4) :: mnist_size
   end type mnist_dataset_t

end module
