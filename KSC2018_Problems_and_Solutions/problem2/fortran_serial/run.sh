#!/bin/bash
#$ -V
#$ -cwd
#$ -N ksc2018_job
#$ -pe mpi_1cpu 1
#$ -q ksc2018
#$ -R yes
#$ -o $JOB_NAME.$JOB_ID.out -j y
#$ -l h_rt=48:00:00
time ./mnist
