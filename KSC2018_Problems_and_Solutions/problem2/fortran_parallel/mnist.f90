program main 
	use neural_network 
	implicit none
    include 'mpif.h'

	! Downloaded from: http://yann.lecun.com/exdb/mnist/ 
	character(len=32) :: train_images_file= "data/train-images-idx3-ubyte" 
	character(len=32) :: train_labels_file = "data/train-labels-idx1-ubyte" 
	character(len=32) :: test_images_file = "data/t10k-images-idx3-ubyte" 
	character(len=32) :: test_labels_file = "data/t10k-labels-idx1-ubyte"
	integer, parameter :: STEPS = 1000
	integer, parameter :: BATCH_SIZE = 400

	type (mnist_dataset_t) train_dataset
	type (mnist_dataset_t) test_dataset
	type (neural_network_t) network
	integer :: i, batches, start_offset, dataset_size 
	real :: loss, accuracy

    integer :: myrank, nproc, ierr
	integer :: ista1, iend1, ista2, iend2

    call mpi_init( ierr )
    call mpi_comm_rank( MPI_COMM_WORLD, myrank, ierr )
    call mpi_comm_size( MPI_COMM_WORLD, nproc, ierr )

	! Read the datasets from the files 
	call mnist_get_dataset(train_images_file, train_labels_file, train_dataset) 
	call mnist_get_dataset(test_images_file, test_labels_file, test_dataset)

	call para_range(BATCH_SIZE, nproc, myrank, ista1, iend1)
	call para_range(test_dataset%mnist_size, nproc, myrank, ista2, iend2)

	! Initialise weights and biases with random values
	call neural_network_random_weights( network )

	batches = train_dataset%mnist_size / BATCH_SIZE;
	do i=0, STEPS-1 
		! Initialise a new batch
		dataset_size = iend1 - ista1 + 1
		start_offset = BATCH_SIZE * mod(i, batches)
   		if (start_offset+dataset_size.gt.train_dataset%mnist_size) then
       		dataset_size = dataset_size - start_offset
   		endif

		! Run one step of gradient descent and calculate the loss
		call neural_network_training_step( train_dataset, start_offset, network, 0.5, ista1, iend1, BATCH_SIZE, loss )

		! Calculate the accuracy using the whole test dataset 
		call calculate_accuracy(test_dataset, network, accuracy, ista2, iend2);

		if (myrank.eq.0) write(*,'(A, I4, A, F0.8, A, F0.8)') "Step ", i, " Average Loss: ", loss / BATCH_SIZE, " Accuracy: ", accuracy
	enddo

	call mnist_free_dataset( train_dataset ) 
	call mnist_free_dataset( test_dataset )

    call mpi_finalize(ierr)

end program
! ================================================================
! para range
! ================================================================
subroutine para_range( N, ranks, myid, ista, iend)
    implicit none
	integer :: N, ranks, myid, ista, iend
	integer :: iwork1, iwork2
	
	iwork1 = N / ranks
	iwork2 = mod(N, ranks)
	ista = myid * iwork1 + min(myid, iwork2)
	iend = ista + iwork1 - 1 
	if (iwork2.gt.myid) iend = iend + 1
end subroutine

! ================================================================
! Convert from the big endian format in the dataset if we're on a little endian
! machine.
! ================================================================
subroutine map_integer32( input )
    implicit none
    integer(kind=4) :: input
    integer :: a1, a2, a3, a4

    a1 = RSHIFT(IAND(input, Z'FF000000'), 24)
    a2 = RSHIFT(IAND(input, Z'00FF0000'), 8)
    a3 = LSHIFT(IAND(input, Z'0000FF00'), 8)
    a4 = LSHIFT(IAND(input, Z'000000FF'), 24)
    input = IOR(IOR(IOR(a1,a2),a3),a4)
end subroutine

! ================================================================
! Read images and labels from file.
! File format: http://yann.lecun.com/exdb/mnist/
! ================================================================
subroutine mnist_get_dataset( image_path, label_path, dataset )
    use mnist_file
    implicit none
    character(len=32) :: image_path
    character(len=32) :: label_path
    type (mnist_dataset_t) dataset
    
    integer(kind=4) :: magic_number
    integer(kind=4) :: number_of_labels
    integer(kind=4) :: number_of_images
    integer(kind=4) :: number_of_rows
    integer(kind=4) :: number_of_columns
    integer :: i,j,x

    open(unit=3, file=image_path, FORM='UNFORMATTED', access='stream')
    read(3) magic_number, number_of_images, number_of_rows, number_of_columns
    call map_integer32(magic_number)
    call map_integer32(number_of_images)
    call map_integer32(number_of_rows)
    call map_integer32(number_of_columns)
    if (magic_number.ne.MNIST_IMAGE_MAGIC) then
        write(*,*) 'Invalid header read from image file: ', image_path 
        call exit(1)
    endif
    allocate(dataset%images(0:number_of_images-1))
    read(3) dataset%images
    close(3)
    dataset%mnist_size = number_of_images 

    open(unit=3, file=label_path, FORM='UNFORMATTED', access='stream')
    read(3) magic_number, number_of_labels
    call map_integer32(magic_number)
    call map_integer32(number_of_labels)
     if (magic_number.ne.MNIST_LABEL_MAGIC) then
        write(*,*) 'Invalid header read from label file: ', image_path 
        call exit(1)
    endif
    allocate(dataset%labels(0:number_of_images-1))
    read(3) dataset%labels
    close(3)

end subroutine
! ================================================================
! Free all the memory allocated in a dataset. This should not be used on a
! batched dataset as the memory is allocated to the parent.
! ================================================================
subroutine mnist_free_dataset( dataset ) 
	use mnist_file 
	implicit none 
	type (mnist_dataset_t) dataset 
	deallocate(dataset%images) 
	deallocate(dataset%labels)
end subroutine
! ================================================================
! Initialise the weights and bias vectors with values between 0 and 1
! ================================================================
subroutine neural_network_random_weights( network )
	use neural_network 
	implicit none 
	type (neural_network_t) network
	integer :: i,j
	do i=0,MNIST_LABELS-1 
	   network%b(i) = rand() 
	   do j=0,MNIST_IMAGE_SIZE-1
	      network%W(i,j) = rand() 
		enddo
	enddo
end subroutine
! ================================================================
! Run one step of gradient descent and update the neural network.
! ================================================================
subroutine neural_network_training_step( dataset,start_offset,network,learning_rate,ista,iend,total_size,loss )
	use neural_network 
	implicit none 
	include 'mpif.h'
	type (mnist_dataset_t) dataset
	integer :: start_offset
	type (neural_network_t) network 
	real :: learning_rate 
	integer :: ista, iend, total_size
	real :: loss

	real :: local_loss, total_loss
	type (neural_network_gradient_t) gradient
	integer :: i, j, ierr

	local_loss = 0
	gradient%b_grad(:) = 0.0
	gradient%W_grad(:,:) = 0.0

	do i=start_offset+ista, start_offset+iend
		call neural_network_gradient_update(dataset, i, network, gradient, dataset%labels(i), loss)
		local_loss = local_loss + loss
	enddo
	call mpi_allreduce(local_loss, total_loss, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD, ierr)
	call mpi_allreduce(MPI_IN_PLACE, gradient%W_grad, MNIST_LABELS*MNIST_IMAGE_SIZE, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD, ierr)
	call mpi_allreduce(MPI_IN_PLACE, gradient%b_grad, MNIST_LABELS, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD, ierr)
	
	do i=0,MNIST_LABELS-1
		network%b(i) = network%b(i) - learning_rate * gradient%b_grad(i) / total_size
		do j=0,MNIST_IMAGE_SIZE-1
			network%W(i,j) = network%W(i,j) - learning_rate * gradient%W_grad(i,j) / total_size
		enddo
	enddo

	loss = total_loss
end subroutine
! ================================================================
! Update the gradients for this step of gradient descent using the gradient
! contributions from a single training example (image).
! This subroutine returns the loss ontribution from this training example
! ================================================================
subroutine neural_network_gradient_update( dataset, dataset_index, network, gradient, label, loss )
	use neural_network
    implicit none
    type (mnist_dataset_t) dataset
	integer :: dataset_index
    type (neural_network_t) network
	type (neural_network_gradient_t) gradient
	character :: label
	real :: loss
	
	real :: activations (0:MNIST_LABELS-1)
	real :: b_grad, W_grad
	integer :: i, j, ilabel

	ilabel = ichar(label)
    ! First forward propagate through the network to calculate activations
	call neural_network_hypothesis(dataset, dataset_index, network, activations)

	do i=0, MNIST_LABELS-1
        ! This is the gradient for a softmax bias input
		if (i.eq.ilabel) then 
			b_grad = activations(i) - 1
		else 
			b_grad = activations(i)
		endif
		do j=0, MNIST_IMAGE_SIZE-1
            ! The gradient for the neuron weight is the bias multiplied by the input weight
			W_grad = b_grad * (ichar(dataset%images(dataset_index)%pixels(j))/255.0);
			! Update the weight gradient
			gradient%W_grad(i, j) = gradient%W_grad(i, j) + W_grad
		enddo
		! Update the bias gradient
		gradient%b_grad(i) = gradient%b_grad(i) + b_grad
	enddo
	! Cross entropy loss
	loss = -log(activations(ilabel))

end subroutine
! ================================================================
! Use the weights and bias vector to forward propogate through the neural
! network and calculate the activations.
! ================================================================
subroutine neural_network_hypothesis( dataset, dataset_index, network, activations )
	use neural_network
    implicit none
    type (mnist_dataset_t) dataset
	integer :: dataset_index
    type (neural_network_t) network
	real :: activations (0:MNIST_LABELS-1)

	integer :: i, j

	do i=0,MNIST_LABELS-1
		activations(i) = network%b(i)
		do j=0,MNIST_IMAGE_SIZE-1
		activations(i) = activations(i) + network%W(i,j) * (ichar(dataset%images(dataset_index)%pixels(j))/255.0)
		enddo
	enddo

	call neural_network_softmax( activations, MNIST_LABELS )

end subroutine
! ================================================================
! Calculate the softmax vector from the activations. This uses a more
! numerically stable algorithm that normalises the activations to prevent
! large exponents.
! ================================================================
subroutine neural_network_softmax( activations, length )
	use neural_network
    implicit none
	real :: activations (0:MNIST_LABELS-1)
	integer :: length 

	integer :: i
	real :: sum_value, max_value

	max_value = activations(0)
	do i=1, length-1
		if (activations(i) > max_value) max_value = activations(i)
	enddo

	sum_value = 0
	do i=0, length-1
		activations(i) = exp(activations(i) - max_value)
		sum_value = sum_value + activations(i)
	enddo

	do i=0, length-1
		activations(i) = activations(i)/sum_value
	enddo

end subroutine
! ================================================================
! Calculate the accuracy of the predictions of a neural network on a dataset.
! ================================================================
subroutine calculate_accuracy( dataset, network, accuracy, ista2, iend2 )
	use neural_network
    implicit none
	include "mpif.h"
    type (mnist_dataset_t) dataset
    type (neural_network_t) network
	real :: accuracy
	integer :: ista2, iend2

	real :: activations (0:MNIST_LABELS-1), max_activation
	integer i, j, correct, predict, total_correct

	correct = 0
	! Loop through the dataset
	do i=ista2, iend2
		! Calculate the activations for each image using the neural network
    	call neural_network_hypothesis(dataset, i, network, activations)
		predict = 0
		max_activation = activations(0)
		! Set predict to the index of the greatest activation
		do j=0, MNIST_LABELS-1
			if (max_activation.lt.activations(j)) then
				max_activation = activations(j)
				predict = j
			endif
		enddo
		! Increment the correct count if we predicted the right label	
		if (predict.eq.ichar(dataset%labels(i))) correct=correct+1
	enddo
	call mpi_allreduce(correct, total_correct, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD)
	! Return the percentage we predicted correctly as the accuracy
	accuracy = real(total_correct) / real(dataset%mnist_size)

end subroutine
