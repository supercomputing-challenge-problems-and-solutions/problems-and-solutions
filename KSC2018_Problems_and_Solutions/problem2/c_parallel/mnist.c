#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

#include "include/mnist_file.h"
#include "include/neural_network.h"

#include "mpi.h"

#define STEPS 1000
#define BATCH_SIZE 400

/**
 * Downloaded from: http://yann.lecun.com/exdb/mnist/
 */
const char * train_images_file = "data/train-images-idx3-ubyte";
const char * train_labels_file = "data/train-labels-idx1-ubyte";
const char * test_images_file = "data/t10k-images-idx3-ubyte";
const char * test_labels_file = "data/t10k-labels-idx1-ubyte";

/**
 * Calculate the accuracy of the predictions of a neural network on a dataset.
 */
float calculate_accuracy(mnist_dataset_t * dataset, neural_network_t * network, int ista2, int iend2)
{
    float activations[MNIST_LABELS], max_activation, local_max;
    int i, j, correct, total_correct, predict;

    struct act_type {
        float activation;
        int   predict;
    } local_res, global_res;

	correct = 0;
    // Loop through the dataset
    for (i = ista2; i <= iend2; i++) {
        // Calculate the activations for each image using the neural network
        neural_network_hypothesis(&dataset->images[i], network, activations);

        // Set predict to the index of the greatest activation
        for (j = 0, local_res.predict = 0, local_res.activation = activations[0]; j < MNIST_LABELS; j++) {
            if (local_res.activation < activations[j]) {
                local_res.activation = activations[j];
                local_res.predict = j;
            }
        }

        // Increment the correct count if we predicted the right label
        if (local_res.predict == dataset->labels[i]) {
            correct++;
        }
    }
    MPI_Allreduce(&correct, &total_correct, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    // Return the percentage we predicted correctly as the accuracy
    return ((float) total_correct) / ((float) dataset->size);
}

void para_range(int N, int ranks, int myid, int *ista, int *iend)
{
	int iwork1;
	int iwork2;
	iwork1 = N / ranks;
	iwork2 = N % ranks;
	*ista = myid * iwork1 + fmin(myid, iwork2); *iend = *ista + iwork1 -1;
	if ( iwork2 > myid) *iend = *iend+ 1;
}

int main(int argc, char *argv[])
{
    mnist_dataset_t * train_dataset, * test_dataset;
    mnist_dataset_t batch;
    neural_network_t network;
    float loss, accuracy;
    int i, batches;
    unsigned long micros = 0;
    float millis = 0.0;
    clock_t start, end;

	int myid, ista1, iend1, ista2, iend2;
	unsigned int total_ranks;

	MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &total_ranks);  // Get # processes
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);      // Get my rank (id)

    // Read the datasets from the files
    train_dataset = mnist_get_dataset(train_images_file, train_labels_file);
    test_dataset = mnist_get_dataset(test_images_file, test_labels_file);

    // Calculate how many batches (so we know when to wrap around)
    batches = train_dataset->size / BATCH_SIZE;

	para_range(BATCH_SIZE, total_ranks, myid, &ista1, &iend1);
	para_range(test_dataset->size, total_ranks, myid, &ista2, &iend2);

	srand(0);
    // Initialise weights and biases with random values
    neural_network_random_weights(&network);

    for (i = 0; i < STEPS; i++) {
        // Initialise a new batch
        mnist_batch(train_dataset, &batch, BATCH_SIZE, iend1-ista1+1, i % batches);

        // Run one step of gradient descent and calculate the loss
        loss = neural_network_training_step(&batch, &network, 0.5, ista1, iend1, BATCH_SIZE);

        // Calculate the accuracy using the whole test dataset
        accuracy = calculate_accuracy(test_dataset, &network, ista2, iend2);

		if (myid==0)
        	printf("Step %04d\tAverage Loss: %.8f\tAccuracy: %.8f\n", i, loss / BATCH_SIZE, accuracy);
    }

	MPI_Finalize();

    // Cleanup
    mnist_free_dataset(train_dataset);
    mnist_free_dataset(test_dataset);

    return 0;
}
