#!/bin/bash
#$ -V
#$ -cwd
#$ -N ksc2018_job
#$ -pe mpi_8cpu 32
#$ -q ksc2018@tachyon0443,ksc2018@tachyon0444,ksc2018@tachyon0445,ksc2018@tachyon0446
#$ -R yes
#$ -o $JOB_NAME.$JOB_ID.out -j y
#$ -l h_rt=48:00:00
time mpirun -machinefile $TMPDIR/machines -np $NSLOTS ./mnist
