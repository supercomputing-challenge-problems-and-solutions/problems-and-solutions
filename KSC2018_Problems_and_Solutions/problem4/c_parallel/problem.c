#include "problem.h"
#include "mpi.h"
int comm_size;
int comm_rank;

// define new type to deliver MolGraph type easier
MPI_Datatype graph_type;


int* recv_tags;
int* recv_flags;
MPI_Request* recv_requests;
MPI_Status* recv_statuses;

int* send_flags;
MPI_Request* send_requests;
MPI_Status* send_statuses;

// for index
MPI_Status status1; 
MPI_Request request1;
int flag1=0; 

// for chemical graph
MPI_Status status2;
MPI_Request request2;
int flag2=0;

// count number of search to be called
int count_search = 0;
int previous_rank=1;

int search (int index, int arr_size, struct MolGraph test_graph){

    const int i = index/test_graph.total_num_atoms;
    const int j = index%test_graph.total_num_atoms;

    if (index >=test_graph.total_num_atoms*test_graph.total_num_atoms ){
        // end search and send messages to other processors 
        return arr_size;
    }
    if(i<=j){
        count_search++;
        // pass this case
        // BO matrix is always symmetric so only half of matrix will be considered
        // and atom having chemical bond to itself is not allowed so i==j case is paseed
        return search(index+1, arr_size, test_graph);
    }

    int* col_sum = (int*) malloc(sizeof(int)*MAX_NUM_ALL_ATOMS);
    get_col_sum(test_graph.total_num_atoms, test_graph.BO_matrix, col_sum );

    if(check_validity(test_graph, col_sum)){
        //list_graph = (struct MolGraph*) realloc(list_graph, sizeof(struct MolGraph)*(arr_size+1));
        //list_graph[arr_size]=test_graph;
        //printf("find!!\n");
        free(col_sum);
        return arr_size+1;
    }

    if( col_sum[i] >= num_chemical_bond[get_element(test_graph,i)] || 
        col_sum[j] >= num_chemical_bond[get_element(test_graph,j)] ){
        free(col_sum);
        count_search++;
        return search(index+1, arr_size, test_graph);
    }
    else{
        free(col_sum);
    }

    test_graph.BO_matrix[i*test_graph.total_num_atoms+j]+=1;
    test_graph.BO_matrix[j*test_graph.total_num_atoms+i]+=1;

    if (comm_rank==0){
        int current_rank=previous_rank;
        for(  ; current_rank < previous_rank+comm_size; current_rank++){   
            int j=current_rank%comm_size;

            if (j==0){
                continue;
            }

            MPI_Test(&recv_requests[j],&recv_flags[j],&recv_statuses[j]);

            if (recv_flags[j]){
                MPI_Isend(&index, 1, MPI_INT, j, comm_size+1, MPI_COMM_WORLD, &request1);
                MPI_Isend(&test_graph, 1, graph_type, j, comm_size+2, MPI_COMM_WORLD, &request2);

                recv_flags[j]=0;

                MPI_Irecv(&recv_tags[j], 1, MPI_INT, j, j, MPI_COMM_WORLD, &recv_requests[j] );
                previous_rank=j+1;
                break;
            }
        }
        if(current_rank==(previous_rank+comm_size)){
            previous_rank=1;
            count_search++;
            arr_size = search(index, arr_size, test_graph);
        }
    }
    else{
        count_search++;
        arr_size = search(index, arr_size, test_graph);
    }
    test_graph.BO_matrix[i*test_graph.total_num_atoms+j]-=1;
    test_graph.BO_matrix[j*test_graph.total_num_atoms+i]-=1;

    count_search++;
    return search(index+1,arr_size,test_graph);    
}

int main(int argc, char* argv[]){

    MPI_Init(&argc,&argv);

    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

    // initialize tag 
    recv_tags = (int*) malloc(sizeof(int)*comm_size);
    memset(recv_tags,0,sizeof(int)*comm_size);

    
    send_flags = (int*) malloc(sizeof(int)*comm_size);
    memset(send_flags,0,sizeof(int)*comm_size);
    send_requests = (MPI_Request*) malloc( sizeof(MPI_Request)*comm_size);
    send_statuses = (MPI_Status*)  malloc(sizeof(MPI_Status)*comm_size);


    recv_flags = (int*) malloc(sizeof(int)*comm_size);
    memset(recv_flags,0,sizeof(int)*comm_size);
    recv_requests = (MPI_Request*) malloc( sizeof(MPI_Request)*comm_size);
    recv_statuses = (MPI_Status*)  malloc(sizeof(MPI_Status)*comm_size);
    
    if(argc !=5){
        printf("please give number of H,C,N,O atoms.\n");    
        exit(-1);
    }

    struct MolGraph initial_graph;

    initial_graph.num_atoms[H] = atoi(argv[1]); 
    initial_graph.num_atoms[C] = atoi(argv[2]); 
    initial_graph.num_atoms[N] = atoi(argv[3]); 
    initial_graph.num_atoms[O] = atoi(argv[4]); 
    memset(initial_graph.BO_matrix, 0, sizeof(int)*MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS);

    initial_graph.total_num_atoms = initial_graph.num_atoms[H]+initial_graph.num_atoms[C]
                                    +initial_graph.num_atoms[N]+initial_graph.num_atoms[O];

    MPI_Datatype types[3] = {MPI_INT,MPI_INT,MPI_INT};
    int blocklengths[3] = {1,MAX_NUM_ELEMENTS,MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS};
    MPI_Aint offsets[3] ;
    MPI_Aint base;

    MPI_Get_address(&initial_graph, &base);
    MPI_Get_address(&initial_graph.total_num_atoms , &offsets[0]);
    MPI_Get_address(&initial_graph.num_atoms[0], &offsets[1] );
    MPI_Get_address(&initial_graph.BO_matrix[0], &offsets[2] );

    offsets[0] -= base;
    offsets[1] -=base;
    offsets[2] -=base;

    MPI_Type_create_struct( 3, blocklengths, offsets, types, &graph_type);
    MPI_Type_commit(&graph_type);

    int initial_index=0;
    bool master_end = false;

    if(comm_rank!=0){
        // try to recieve end_tag from master processor 
        MPI_Irecv(&recv_tags[0], 1 ,MPI_INT, 0, comm_size, MPI_COMM_WORLD, &recv_requests[0]);

        // send end_tag to master processor
        MPI_Isend(&one, 1, MPI_INT, 0, comm_rank, MPI_COMM_WORLD, &send_requests[0]);

        // try to recieve initial index and graph 
        MPI_Irecv(&initial_index, 1, MPI_INT, 0, comm_size+1, MPI_COMM_WORLD, &request1);
        MPI_Irecv(&initial_graph, 1, graph_type, 0, comm_size+2, MPI_COMM_WORLD, &request2);

        // wait until initial index and graph are successfully recieved
        while(1){

            MPI_Test(&recv_requests[0], &recv_flags[0] , &recv_statuses[0]);
            if(recv_flags[0]){
                recv_flags[0]=0;
                master_end=true;
                break;
            }

            MPI_Test(&request1,&flag1,&status1);
            MPI_Test(&request2,&flag2,&status2);
            if(flag1 && flag2){
                flag1=0; flag2=0;
                break;
            }
        }
    }
    else{
        // try to recieve end_tags from slave processors
        for(int i=1; i<comm_size; i++){
            MPI_Irecv( &recv_tags[i] ,1, MPI_INT,i, i, MPI_COMM_WORLD,&recv_requests[i]);
        }
    }

    int size=0;

    if(comm_rank==0){
        count_search++;
        size = search(initial_index,0,initial_graph);
        for(int i=1; i< comm_size;i++ ){
            MPI_Isend(&one, 1, MPI_INT, i, comm_size, MPI_COMM_WORLD, &send_requests[i]);
        }
    }
    else{
        while (master_end==false){
            count_search++;
            size += search(initial_index,0,initial_graph);
            // send finish tag to master processor
            MPI_Isend(&one, 1, MPI_INT, 0, comm_rank, MPI_COMM_WORLD, &send_requests[0]);
            // and try to recieve new index and graph
            MPI_Irecv(&initial_index, 1, MPI_INT, 0, comm_size+1, MPI_COMM_WORLD, &request1);
            MPI_Irecv(&initial_graph, 1, graph_type, 0, comm_size+2, MPI_COMM_WORLD, &request2);
 
            while(1){
                // check whether master processor reach end 
                MPI_Test(&recv_requests[0], &recv_flags[0] , &recv_statuses[0]);
                if(recv_flags[0]){
                    recv_flags[0]=0;
                    master_end=true;
                    break;
                }
                MPI_Test(&request1,&flag1,&status1);
                MPI_Test(&request2,&flag2,&status2);
                if(flag1 && flag2){
                    flag1=0; flag2=0;
                    break;
                }
            }
        }
    }
    // sum up number of found graphs and print the result
    int total_size; int total_count;
    MPI_Reduce(&size,  &total_size, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&count_search, &total_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (comm_rank==0){
        printf("size : %d\n", total_size);
        printf("count: %d\n", total_count);
    }
//    printf("====================\n");
//    for (int i =0; i<size; i++){
//        print_graph(list_graph[i]);
//        printf("\n");
//    }

    free(recv_tags);
    free(recv_flags);
    free(recv_requests);

    free(send_flags);
    free(send_requests);
    MPI_Finalize();

    return 0;
}
