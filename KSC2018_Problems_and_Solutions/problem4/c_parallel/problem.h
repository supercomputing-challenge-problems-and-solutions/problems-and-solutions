#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_NUM_ALL_ATOMS 12
#define MAX_NUM_ELEMENTS 4

const int one =1;

int num_chemical_bond[MAX_NUM_ELEMENTS]={1,4,3,2};

enum chem_symbol{
    H, C, N,O
};

struct MolGraph{
    int total_num_atoms;
    int num_atoms[MAX_NUM_ELEMENTS];
    int BO_matrix [MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS];
};

int get_element(struct MolGraph graph, int index){
    if (index<graph.num_atoms[H])
        return H;
    if (index<graph.num_atoms[H]+graph.num_atoms[C])
        return C;
    if (index<graph.num_atoms[H]+graph.num_atoms[C]+graph.num_atoms[N])
        return N;
    return O;
}

void get_col_sum(int size,int* BO_matrix, int* result){
    memset(result,0,sizeof(int)*MAX_NUM_ALL_ATOMS);
    for (int i=0; i<size; i++){
        for (int j=0; j<size; j++){
            result[i]+=BO_matrix[i*size+j];
        }
    }
    return;
}

bool check_validity(struct MolGraph test_graph, int* col_sum){
    for (int i=0; i<test_graph.total_num_atoms;i++){
        if(col_sum[i] != num_chemical_bond[get_element(test_graph,i)]){
            return false;
        }
    }
    return true;
}

void print_graph(struct MolGraph graph){
    printf("  ");
    for(int i=0; i<graph.num_atoms[H]; i++){
        printf("H ");
    }
    for(int i=0; i<graph.num_atoms[C]; i++){
        printf("C ");
    }
    for(int i=0; i<graph.num_atoms[N]; i++){
        printf("N ");
    }
    for(int i=0; i<graph.num_atoms[O]; i++){
        printf("O ");
    }
    printf("\n");

    for (int i=0; i<graph.num_atoms[H];i++){
        printf("H ");
        for (int j=0; j<graph.total_num_atoms; j++){
            printf("%01d ", graph.BO_matrix[i*graph.total_num_atoms+j]);
        }
        printf("\n");
    }
    for (int i=graph.num_atoms[H]; i<graph.num_atoms[H]+graph.num_atoms[C];i++){
        printf("C ");
        for (int j=0; j<graph.total_num_atoms; j++){
            printf("%01d ", graph.BO_matrix[i*graph.total_num_atoms+j]);
        }
        printf("\n");
    }
    for (int i=graph.num_atoms[H]+graph.num_atoms[C]; i<graph.num_atoms[H]+graph.num_atoms[C]+graph.num_atoms[N];i++){
        printf("N ");
        for (int j=0; j<graph.total_num_atoms; j++){
            printf("%01d ", graph.BO_matrix[i*graph.total_num_atoms+j]);
        }
        printf("\n");
    }
    for (int i=graph.num_atoms[H]+graph.num_atoms[C]+graph.num_atoms[N]; i<graph.total_num_atoms;i++){
        printf("O ");
        for (int j=0; j<graph.total_num_atoms; j++){
            printf("%01d ", graph.BO_matrix[i*graph.total_num_atoms+j]);
        }
        printf("\n");
    }
    return;
}
