#include "problem.h"
int count = 0;

int search (int index, int arr_size, struct MolGraph test_graph){

    const int i = index/test_graph.total_num_atoms;
    const int j = index%test_graph.total_num_atoms;

    if (index >=test_graph.total_num_atoms*test_graph.total_num_atoms ){
        // end search and send messages to other processors 
        return arr_size;
    }
    if(i<=j){
        count++;
        // pass this case
        // BO matrix is always symmetric so only half of matrix will be considered
        // and atom having chemical bond to itself is not allowed so i==j case is paseed
        return search(index+1, arr_size, test_graph);
    }

    int* col_sum = (int*) malloc(sizeof(int)*MAX_NUM_ALL_ATOMS);
    get_col_sum(test_graph.total_num_atoms, test_graph.BO_matrix, col_sum );

    if(check_validity(test_graph, col_sum)){
        free(col_sum);
        return arr_size+1;
    }

    if( col_sum[i] >= num_chemical_bond[get_element(test_graph,i)] || 
        col_sum[j] >= num_chemical_bond[get_element(test_graph,j)] ){
        free(col_sum);

        count++;
        return search(index+1, arr_size, test_graph);
    }
    else{
        free(col_sum);
    }
    // Now, increase number of bond at index position and start next step of searching
    // Because of symmetry of BO matrix, we need to change two element of matrix
    test_graph.BO_matrix[i*test_graph.total_num_atoms+j]+=1;
    test_graph.BO_matrix[j*test_graph.total_num_atoms+i]+=1;

    count++;
    arr_size = search(index, arr_size, test_graph);

    // recover the original BO matrix and start next step of searching with increased index value
    test_graph.BO_matrix[i*test_graph.total_num_atoms+j]-=1;
    test_graph.BO_matrix[j*test_graph.total_num_atoms+i]-=1;

    count++;
    return search(index+1,arr_size,test_graph);    
}

int main(int argc, char* argv[]){

    if(argc !=5){
        printf("please give number of H,C,N,O atoms.\n");    
        exit(-1);
    }

    struct MolGraph initial_graph;

    initial_graph.num_atoms[H] = atoi(argv[1]); 
    initial_graph.num_atoms[C] = atoi(argv[2]); 
    initial_graph.num_atoms[N] = atoi(argv[3]); 
    initial_graph.num_atoms[O] = atoi(argv[4]); 
    memset(initial_graph.BO_matrix, 0, sizeof(int)*MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS);

    initial_graph.total_num_atoms = initial_graph.num_atoms[H]+initial_graph.num_atoms[C]
                                    +initial_graph.num_atoms[N]+initial_graph.num_atoms[O];

    int initial_index=0;
    count++;
    int size = search(initial_index,0,initial_graph);
    printf("size : %d\n", size);
    printf("count: %d\n", count );


    return 0;
}
