module parallel
    use mpi
    implicit none
    integer :: comm_size, comm_rank
    integer,parameter :: one = 1

    integer, allocatable, dimension(:) :: recv_tags
    logical, allocatable, dimension(:) :: recv_flags
    integer, allocatable, dimension(:) :: recv_requests
    integer, allocatable, dimension(:,:) :: recv_statuses

    logical, allocatable, dimension(:) :: send_flags
    integer, allocatable, dimension(:) :: send_requests
    integer, allocatable, dimension(:,:) :: send_statuses


    integer :: status1(MPI_STATUS_SIZE) 
    integer :: request1
    logical flag1

    integer :: status2(MPI_STATUS_SIZE) 
    integer :: request2
    logical :: flag2

    integer :: current_rank, previous_rank = 1
    integer :: rank
    integer :: ierr
    integer :: graph_type
end module 
module utility
    implicit none
    integer,parameter :: MAX_NUM_ELEMENTS  = 4
    integer,parameter :: MAX_NUM_ALL_ATOMS = 12

    integer,parameter :: H = 1
    integer,parameter :: C = 2
    integer,parameter :: N = 3
    integer,parameter :: O = 4

    integer,parameter :: num_chemical_bond(4) = (/ 1, 4, 3, 2 /)

    INTEGER :: count_search =0

    type :: MolGraph
        integer :: total_num_atoms
        integer :: num_atoms(MAX_NUM_ELEMENTS)
        integer :: BO_matrix (MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS)
    end type  MolGraph
    contains
    subroutine print_graph( graph)
        implicit none
        type(MolGraph),intent(in) :: graph
        integer i,j
        write(*,"(A)",advance="no") "  "
        do i = 1,  graph%num_atoms(1)
            write(*,"(A)",advance="no") "H "
        end do 
        do i = 1,  graph%num_atoms(2)
            write(*,"(A)",advance="no") "C "
        end do 
        do i = 1,  graph%num_atoms(3)
            write(*,"(A)",advance="no") "N "
        end do 
        do i = 1,  graph%num_atoms(4)
            write(*,"(A)",advance="no") "O "
        end do
        write(*,*) "" 

        do i = 1,  graph%num_atoms(1)
            write(*,"(A)",advance="no") "H "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '
                
            end do
            write(*,*) "" 
        end do

        do i = graph%num_atoms(1)+1, graph%num_atoms(1)+graph%num_atoms(2)
            write(*,"(A)",advance="no") "C "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do

        do i = graph%num_atoms(1)+graph%num_atoms(2)+1, &
                graph%num_atoms(1)+graph%num_atoms(2)+ graph%num_atoms(3)
            write(*,"(A)",advance="no") "N "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do

        do i = graph%num_atoms(1)+graph%num_atoms(2)+ graph%num_atoms(3)+1,&
                 graph%total_num_atoms
            write(*,"(A)",advance="no") "O "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do
        return 
    end subroutine 

    subroutine get_col_sum(total_num_atoms, BO_matrix, col_sum)
        implicit none
        integer, intent(in) :: total_num_atoms
        integer,intent(in)::BO_matrix(MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS)
        integer, intent(out) :: col_sum(MAX_NUM_ALL_ATOMS)
        integer :: i,j
        col_sum(:) = 0
        do i =1,total_num_atoms
            do j=1,total_num_atoms
                col_sum(i) = col_sum(i) + BO_matrix((i-1)*total_num_atoms+j)
            end do
        end do
        return 
    end subroutine 

    subroutine get_element(graph, ind, return_val)
        implicit none
        integer, intent(in) :: ind
        type(MolGraph), intent(in) :: graph 
        integer, intent(out) :: return_val
        if(ind <= graph%num_atoms(H)) then
            return_val = H
            return
        endif 
        if(ind <= graph%num_atoms(H)+graph%num_atoms(C)) then
            return_val = C
            return
        endif 
        if(ind <= &
            graph%num_atoms(H)+graph%num_atoms(C)+graph%num_atoms(N)) &
            then
            return_val = N
            return
        endif 
        return_val = O
        return
    end subroutine 

    subroutine check_validity(test_graph, col_sum, return_val)
        implicit none
        type(MolGraph),intent(in) :: test_graph
        integer, intent(in) :: col_sum(MAX_NUM_ALL_ATOMS)
        integer :: element
        logical, intent(out) :: return_val 
        integer i
        return_val= .true.
        do i = 1, test_graph%total_num_atoms
            call get_element(test_graph,i, element)
            if (col_sum(i) /= num_chemical_bond(element) ) &
                then 
                return_val = .false.
                exit
            endif
        end do 
        return 
    end subroutine

end module

recursive function search(ind, arr_size, in_graph) result(return_val)
    use utility 
    use mpi
    use parallel
    implicit none
    type(MolGraph),intent(in) :: in_graph
    integer, intent(in) :: ind
    integer, intent(in) :: arr_size
    !integer, intent(in) :: arr_size
    type(MolGraph) :: test_graph
    integer :: return_val 
    integer :: i,j,element1,element2
    integer :: ii,jj
    integer :: col_sum(MAX_NUM_ALL_ATOMS)= 0
    logical :: is_valid, my_turn = .true.
    integer :: arr_size_tmp
    arr_size_tmp = arr_size
    test_graph%total_num_atoms = in_graph%total_num_atoms
    test_graph%num_atoms       = in_graph%num_atoms
    test_graph%BO_matrix       = in_graph%BO_matrix


    i = ind / (test_graph%total_num_atoms) +1
    j = mod(ind,test_graph%total_num_atoms) +1 


    if(ind >=test_graph%total_num_atoms*test_graph%total_num_atoms) then
        return_val = arr_size_tmp
        return 
    endif 

    if (i<=j) then
        count_search = count_search + 1
        return_val = search(ind+1,arr_size_tmp,test_graph)
        return         
    endif 

    call get_col_sum( test_graph%total_num_atoms, test_graph%BO_matrix, col_sum)
    call check_validity(test_graph,col_sum, is_valid)

    if ( is_valid ) then
        return_val=arr_size_tmp+1
        return
    endif
     

    call get_element(test_graph,i,element1)
    call get_element(test_graph,j,element2)
!    call print_graph(test_graph)
    if (col_sum(i)>= num_chemical_bond(element1) .or. &
            col_sum(j) >=num_chemical_bond(element2) ) then
        count_search = count_search + 1
        return_val = search(ind+1,arr_size_tmp,test_graph)
        return 
    endif 

    test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) = &
        test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) + 1

    test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) = &
        test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) + 1

    if(comm_rank == 0) then
        current_rank = previous_rank
!        my_turn = .true.
        do current_rank = previous_rank, previous_rank+comm_size-1
            rank = mod(current_rank,comm_size)
            if(rank==0) then
                cycle
            endif
            call MPI_Test(recv_requests(rank+1),recv_flags(rank+1),recv_statuses(1,rank+1),ierr)
            if(recv_flags(rank+1)) then 
                call MPI_Isend(ind, 1, MPI_INTEGER, rank, comm_size+1,  &
                                MPI_COMM_WORLD, request1, ierr)

                call MPI_Isend(test_graph, 1, graph_type, rank, comm_size+2,&
                                MPI_COMM_WORLD, request2, ierr)
                recv_flags(rank+1) = .false.
                call MPI_Irecv(recv_tags(rank+1), 1, MPI_INTEGER, rank, rank, &
                                MPI_COMM_WORLD, recv_requests(rank+1), ierr)
                previous_rank = rank+1
!                my_turn = .false.
                exit
            endif
        enddo
!        if (my_turn) then 
        if (current_rank==(previous_rank+comm_size)) then 
            previous_rank=1
            count_search = count_search+1
            arr_size_tmp = search(ind, arr_size_tmp, test_graph)
            !return_val = search(ind, arr_size, test_graph)
        endif 
    else
        count_search = count_search +1
        arr_size_tmp = search (ind, arr_size_tmp, test_graph)
        !return_val = search (ind, arr_size, test_graph)
    endif 

    test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) = &
        test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) - 1

    test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) = &
        test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) - 1

    count_search = count_search +1
    return_val = search(ind+1, arr_size_tmp, test_graph)
    return 

end function search


program main
    use utility
    use mpi
    use parallel
    implicit none

    character(len=8) :: arg
    integer :: total_size, my_size
    integer :: total_count, my_count 
    integer :: i, num
    type(MolGraph) :: initial_graph 
    integer,external :: search

    logical :: master_end = .false.
    integer :: initial_index=0

    integer :: types(3) = (/ MPI_INTEGER,MPI_INTEGER,MPI_INTEGER /)

    integer :: blocklengths(3) = (/1,MAX_NUM_ELEMENTS, &
                            MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS/)
    integer(KIND=MPI_ADDRESS_KIND) :: base
    integer(kind=MPI_ADDRESS_KIND):: offsets(3)

    call mpi_init(ierr)

    call mpi_comm_rank(mpi_comm_world, comm_rank, ierr)
    call mpi_comm_size(mpi_comm_world, comm_size, ierr)

    allocate(recv_tags(comm_size))
    allocate(recv_flags(comm_size))
    allocate(recv_requests(comm_size))
    allocate(recv_statuses(MPI_STATUS_SIZE,comm_size))

    allocate(send_flags(comm_size))
    allocate(send_requests(comm_size))
    allocate(send_statuses(MPI_STATUS_SIZE,comm_size))
    ! initialize tags and flags
    recv_tags(:)=0
    recv_flags(:)=.false.
    send_flags(:)=.false.

    if (command_argument_count()/= 4) then 
        write (*,*)'please give number of H,C,N,O atoms.'
        call exit(-1)
    end if 

    initial_graph%total_num_atoms = 0

    ! initialize BO_matrix
    do i =1 ,MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS
        initial_graph%BO_matrix(i) = 0
    end do

    ! put input arguments on num_atoms(i)
    do i = 1, command_argument_count()
        call get_command_argument(i, arg)
        read(arg, *) num ! type casting
        initial_graph%num_atoms(i) = num
        initial_graph%total_num_atoms = initial_graph%total_num_atoms &
                                        + initial_graph%num_atoms(i)
    end do


    ! construct graph_type 

    call MPI_Get_address(initial_graph, base, ierr)
    call MPI_Get_address(initial_graph%total_num_atoms , offsets(1), ierr)
    call MPI_Get_address(initial_graph%num_atoms(1), offsets(2), ierr )
    call MPI_Get_address(initial_graph%BO_matrix(1), offsets(3), ierr )

    offsets(3) =offsets(3)-base
    offsets(2) =offsets(2)-base
    offsets(1) =offsets(1)-base
    
    call MPI_Type_create_struct( 3, blocklengths, offsets, types, graph_type, ierr)
    call MPI_Type_commit(graph_type, ierr)

    if (comm_rank/=0) then
        ! try to receive end_tag from master processor
        call MPI_Irecv(recv_tags(1), 1, MPI_INT, 0, comm_size, &
                        MPI_COMM_WORLD, recv_requests(1), ierr)
        ! send end_tag to master processor
        call MPI_Isend(one, 1, MPI_INT, 0, comm_rank, &
                        MPI_COMM_WORLD, send_requests(1), ierr)
        ! try to recive initial index and graph 
        call MPI_Irecv(initial_index, 1, MPI_INT, 0, comm_size+1, &
                        MPI_COMM_WORLD, request1, ierr)
        call MPI_Irecv(initial_graph, 1, graph_type, 0, comm_size+2, &
                        MPI_COMM_WORLD, request2, ierr)

        ! wait unitl initial index and graph are successfully recieved
        do while(.true.)
            call MPI_Test(recv_requests(1), recv_flags(1), &
                            recv_statuses(1,1), ierr )
            if(recv_flags(1)) then 
                recv_flags(1) = .false.
                master_end = .true.
                exit
            endif 

            call MPI_Test(request1, flag1, status1, ierr)
            call MPI_Test(request2, flag2, status2, ierr)

            if ( flag1 .and. flag2) then 
                flag1 = .false.
                flag2 = .false.
                exit    
            endif 
        enddo 
    else
        ! try to recieve end_tags from slave processors
        do i = 1,comm_size-1
            call MPI_Irecv(recv_tags(i+1), 1, MPI_INTEGER, i, i, &
                        MPI_COMM_WORLD, recv_requests(i+1) , ierr)
        enddo 
    endif 

    my_size = 0
    if (comm_rank==0) then 
        ! send finish tag to slave processors
        count_search = count_search + 1
        my_size = search(0,0,initial_graph)
        do i = 1, comm_size-1
            call MPI_Isend(one, 1, MPI_INTEGER, i, comm_size, MPI_COMM_WORLD,&
                        send_requests(i+1),ierr)
        enddo
    else 
        do while( master_end .eqv. .false.) 
            count_search = count_search + 1 
            my_size = my_size + search(initial_index, 0, initial_graph)

            ! send finish tag to master processor
            call MPI_Isend(one, 1, MPI_INTEGER, 0, comm_rank, MPI_COMM_WORLD,&
                        send_requests(1), ierr)
            
            ! and try to recieve new index and graph
            call MPI_Irecv(initial_index, 1, MPI_INTEGER, 0, comm_size + 1, &
                        MPI_COMM_WORLD, request1, ierr)
            call MPI_Irecv(initial_graph, 1, graph_type, 0, comm_size + 2, &
                        MPI_COMM_WORLD, request2, ierr)

            do while(.true.) 
                ! check whether master processor reach end 
                call MPI_Test(recv_requests(1), recv_flags(1), recv_statuses(1,1),ierr)
                if (recv_flags(1)) then 
                    recv_flags(1) = .false.
                    master_end = .true.
                    exit
                endif 

                call MPI_Test(request1, flag1, status1, ierr)
                call MPI_Test(request2, flag2, status2, ierr)

                if(flag1 .and. flag2) then 
                    flag1 = .false. 
                    flag2 = .false.
                    exit
                endif 
            enddo
        enddo
    endif 


    call MPI_Reduce(my_size, total_size, 1, MPI_INTEGER, MPI_SUM, 0, &
                MPI_COMM_WORLD, ierr)
    call MPI_Reduce(count_search, total_count, 1, MPI_INTEGER, MPI_SUM, 0, &
                MPI_COMM_WORLD, ierr)

    if (comm_rank == 0 ) then 
        write (*,'(A,I9)') "size :",total_size
        write (*,'(A,I9)') "count:",total_count
    endif 

    deallocate(recv_tags,recv_flags,recv_requests,recv_statuses)
    deallocate(send_flags,send_requests,send_statuses)

    call mpi_finalize(ierr)
end program main

