module utility
    integer,parameter :: MAX_NUM_ELEMENTS  = 4
    integer,parameter :: MAX_NUM_ALL_ATOMS = 12

    integer,parameter :: H = 1
    integer,parameter :: C = 2
    integer,parameter :: N = 3
    integer,parameter :: O = 4

    integer,parameter :: num_chemical_bond(4) = (/ 1, 4, 3, 2 /)

    INTEGER :: count_search

    type MolGraph
        integer :: total_num_atoms
        integer :: num_atoms(MAX_NUM_ELEMENTS)
        integer :: BO_matrix (MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS)
    end type  MolGraph
    contains
    subroutine print_graph( graph)
        type(MolGraph),intent(in) :: graph
        integer i,j
        write(*,"(A)",advance="no") "  "
        do i = 1,  graph%num_atoms(1)
            write(*,"(A)",advance="no") "H "
        end do 
        do i = 1,  graph%num_atoms(2)
            write(*,"(A)",advance="no") "C "
        end do 
        do i = 1,  graph%num_atoms(3)
            write(*,"(A)",advance="no") "N "
        end do 
        do i = 1,  graph%num_atoms(4)
            write(*,"(A)",advance="no") "O "
        end do
        write(*,*) "" 

        do i = 1,  graph%num_atoms(1)
            write(*,"(A)",advance="no") "H "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '
                
            end do
            write(*,*) "" 
        end do

        do i = graph%num_atoms(1)+1, graph%num_atoms(1)+graph%num_atoms(2)
            write(*,"(A)",advance="no") "C "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do

        do i = graph%num_atoms(1)+graph%num_atoms(2)+1, &
                graph%num_atoms(1)+graph%num_atoms(2)+ graph%num_atoms(3)
            write(*,"(A)",advance="no") "N "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do

        do i = graph%num_atoms(1)+graph%num_atoms(2)+ graph%num_atoms(3)+1,&
                 graph%total_num_atoms
            write(*,"(A)",advance="no") "O "
            do j = 1, graph%total_num_atoms
                write(*,"(I1,A)",advance="no") &
                    graph%BO_matrix((i-1)*graph%total_num_atoms+j),' '

            end do
            write(*,*) ""
        end do
        return 
    end subroutine 

    subroutine get_col_sum(total_num_atoms, BO_matrix, col_sum)
        integer, intent(in) :: total_num_atoms
        integer,intent(in)::BO_matrix(MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS)
        integer, intent(out) :: col_sum(MAX_NUM_ALL_ATOMS)
        integer :: i,j
        col_sum(:) = 0
        do i =1,total_num_atoms
            do j=1,total_num_atoms
                col_sum(i) = col_sum(i) + BO_matrix((i-1)*total_num_atoms+j)
            end do
        end do
        return 
    end subroutine 

    subroutine get_element(graph, ind, return_val)
        integer, intent(in) :: ind
        type(MolGraph), intent(in) :: graph 
        integer, intent(out) :: return_val
        if(ind <= graph%num_atoms(H)) then
            return_val = H
            return
        end if 
        if(ind <= graph%num_atoms(H)+graph%num_atoms(C)) then
            return_val = C
            return
        end if 
        if(ind <= &
            graph%num_atoms(H)+graph%num_atoms(C)+graph%num_atoms(N)) &
            then
            return_val = N
            return
        end if 
        return_val = O
        return
    end subroutine 

    subroutine check_validity(test_graph, col_sum, return_val)
        type(MolGraph),intent(in) :: test_graph
        integer, intent(in) :: col_sum(MAX_NUM_ALL_ATOMS)
        integer :: element
        logical, intent(out) :: return_val 
        integer i
        return_val= .true.
        do i = 1, test_graph%total_num_atoms
            call get_element(test_graph,i, element)
            if (col_sum(i) /= num_chemical_bond(element) ) &
                then 
                return_val = .false.
                exit
            end if
        end do 
        return 
    end subroutine

end module

recursive function search(ind, arr_size, in_graph) result(return_val)
    use utility 
    type(MolGraph),intent(in) :: in_graph
    integer, intent(in) :: ind
    integer, intent(in) :: arr_size
    type(MolGraph) :: test_graph
    integer :: return_val 
    integer :: i,j,element1,element2
    integer :: ii,jj
    integer :: col_sum(MAX_NUM_ALL_ATOMS)= 0
    logical :: is_valid 

    test_graph%total_num_atoms = in_graph%total_num_atoms
    test_graph%num_atoms       = in_graph%num_atoms
    test_graph%BO_matrix       = in_graph%BO_matrix

!    write(*,*) test_graph%num_atoms(1)
!    write(*,*) test_graph%num_atoms(2)
!    write(*,*) test_graph%num_atoms(3)
!    write(*,*) test_graph%num_atoms(4)
!
!    call exit(-1)
!    write(*,"(A,I3,I3,I3,I3)") "search start(0): ", (test_graph%total_num_atoms) ,ind

    i = ind / (test_graph%total_num_atoms) +1
    j = mod(ind,test_graph%total_num_atoms)  +1

    if(ind >=test_graph%total_num_atoms*test_graph%total_num_atoms) then
        return_val = arr_size
        return 
    end if 

    if (i<=j) then
        count_search = count_search + 1
        return_val = search(ind+1,arr_size,test_graph)
        return         
    end if 

    call get_col_sum( test_graph%total_num_atoms, test_graph%BO_matrix, col_sum)
    call check_validity(test_graph,col_sum, is_valid)

    if ( is_valid ) then
        return_val = arr_size +1
        return
    end if
     
    call get_element(test_graph,i,element1)
    call get_element(test_graph,j,element2)
    if (col_sum(i)>= num_chemical_bond(element1) .or. &
            col_sum(j) >=num_chemical_bond(element2) ) then
        count_search = count_search + 1
        return_val = search(ind+1,arr_size,test_graph)
        return 
    end if 

    test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) = &
        test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) + 1

    test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) = &
        test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) + 1

    count_search = count_search + 1
    return_val = search(ind, arr_size, test_graph)

    test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) = &
        test_graph%BO_matrix((i-1)*test_graph%total_num_atoms+j) - 1

    test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) = &
        test_graph%BO_matrix((j-1)*test_graph%total_num_atoms+i) - 1


    count_search = count_search +1
    return_val = search(ind+1, return_val, test_graph)
    return 

end function search


program main
    use utility
    implicit none

    character(len=8) :: arg
    integer :: total_size
    integer :: i, num
    type(MolGraph) :: initial_graph 
    integer,external :: search

    if (command_argument_count()/= 4) then 
        write (*,*)'please give number of H,C,N,O atoms.'
        call exit(-1)
    end if 

    initial_graph%total_num_atoms = 0

    ! put input arguments on num_atoms(i)
    do i = 1, command_argument_count()
        call get_command_argument(i, arg)
        read(arg, *) num ! type casting
        initial_graph%num_atoms(i) = num
        initial_graph%total_num_atoms = initial_graph%total_num_atoms &
                                        + initial_graph%num_atoms(i)
    end do

    ! initialize BO_matrix
    do i =1 ,MAX_NUM_ALL_ATOMS*MAX_NUM_ALL_ATOMS
        initial_graph%BO_matrix(i) = 0
    end do

    count_search = 1
    total_size = search(0,0,initial_graph)
    write (*,'(A,I9)') "size :",total_size
    write (*,'(A,I9)') "count:",count_search


end program main

