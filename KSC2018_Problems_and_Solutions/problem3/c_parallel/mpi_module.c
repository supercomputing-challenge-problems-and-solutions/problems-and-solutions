#include <stdio.h>
#include <mpi.h>
#include "mpi_module.h"

void mpi_setup(int nxp, int nyp, int nzp, int px, int py, int pz, MYMPI *mpi_info)
{
	int i;

	mpi_info->nx = nxp;
	mpi_info->ny = nyp;
	mpi_info->nz = nzp;
	
	mpi_info->nproc_x = px;
	mpi_info->nproc_y = py;
	mpi_info->nproc_z = pz;
	
	mpi_info->nx_mpi = mpi_info->nx / mpi_info->nproc_x;
	mpi_info->ny_mpi = mpi_info->ny / mpi_info->nproc_y;
	mpi_info->nz_mpi = mpi_info->nz / mpi_info->nproc_z;

	mpi_info->ntotalsize = mpi_info->nx_mpi*mpi_info->nz_mpi*mpi_info->ny;
	mpi_info->nz_blksize = mpi_info->ntotalsize/mpi_info->nproc_x;
	mpi_info->nx_blksize = mpi_info->ntotalsize/mpi_info->nproc_z;
	mpi_info->nz_mpiblk=mpi_info->nz_mpi/mpi_info->nproc_x;
	mpi_info->nx_mpiblk=mpi_info->nx_mpi/mpi_info->nproc_z;

	mpi_info->idx = mpi_info->mpirank/mpi_info->nproc_z;
	mpi_info->idy = 0;
	mpi_info->idz = mpi_info->mpirank - mpi_info->idx*mpi_info->nproc_z;

	mpi_info->ista=mpi_info->nx/mpi_info->nproc_x*mpi_info->idx;
	mpi_info->iend=mpi_info->ista+mpi_info->nx_mpi-1;
	mpi_info->jsta=mpi_info->ny/mpi_info->nproc_y*mpi_info->idy;
	mpi_info->jend=mpi_info->jsta+mpi_info->ny_mpi-1;
	mpi_info->ksta=mpi_info->nz/mpi_info->nproc_z*mpi_info->idz;
	mpi_info->kend=mpi_info->ksta+mpi_info->nz_mpi-1;

	MPI_Comm_split(MPI_COMM_WORLD,mpi_info->idz,mpi_info->idx,&mpi_info->MPI_COMM_X);
	MPI_Comm_split(MPI_COMM_WORLD,mpi_info->idx,mpi_info->idz,&mpi_info->MPI_COMM_Z);

	MPI_Comm_size(mpi_info->MPI_COMM_X,&mpi_info->mpisize_x);
	MPI_Comm_rank(mpi_info->MPI_COMM_X,&mpi_info->mpirank_x);

	MPI_Comm_size(mpi_info->MPI_COMM_Z,&mpi_info->mpisize_z);
	MPI_Comm_rank(mpi_info->MPI_COMM_Z,&mpi_info->mpirank_z);

}

void mpi_info_write(MYMPI *mpi_info)
{
	int i;

	MPI_Barrier(MPI_COMM_WORLD);
    for(i=0;i<mpi_info->mpisize;i++) {
		if(mpi_info->mpirank==i) {
			printf("mpirank=%5d, my mpi id = (%5d, %5d, %5d)\n",mpi_info->mpirank,mpi_info->idx,mpi_info->idy,mpi_info->idz);
			printf("mpirank=%5d, mpi size  = (%5d, %5d, %5d)\n",mpi_info->mpirank,mpi_info->nx_mpi,mpi_info->ny_mpi,mpi_info->nz_mpi);
			printf("mpirank=%5d, ista,iend = (%5d, %5d)\n",mpi_info->mpirank,mpi_info->ista,mpi_info->iend);
			printf("mpirank=%5d, jsta,jend = (%5d, %5d)\n",mpi_info->mpirank,mpi_info->jsta,mpi_info->jend);
			printf("mpirank=%5d, ksta,kend = (%5d, %5d)\n",mpi_info->mpirank,mpi_info->ksta,mpi_info->kend);
			printf("mpirank=%5d, x/z rank  = (%5d, %5d)\n",mpi_info->mpirank,mpi_info->mpirank_x,mpi_info->mpirank_z);
			printf("mpirank=%5d, x/z size  = (%5d, %5d)\n",mpi_info->mpirank,mpi_info->mpisize_x,mpi_info->mpisize_z);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}
void mpi_free(MYMPI *mpi_info)
{
	MPI_Comm_free(&mpi_info->MPI_COMM_X);
	MPI_Comm_free(&mpi_info->MPI_COMM_Z);
}


