#include <stdio.h>
#include <mpi.h>
typedef struct mympi {
	int nproc_x, nproc_y, nproc_z;
	int nx, ny, nz;
	int nx_mpi, ny_mpi, nz_mpi;
	int mpirank, mpisize;
	int mpirank_x, mpirank_z;
	int mpisize_x, mpisize_z;
	int idx, idy, idz;
	int ista, iend, jsta, jend, ksta, kend;
	int ntotalsize, nz_blksize, nx_blksize, nz_mpiblk, nx_mpiblk;
	MPI_Comm MPI_COMM_X, MPI_COMM_Z;
} MYMPI;

void mpi_setup(int nxp, int nyp, int nzp, int px, int py, int pz, MYMPI *mpi_info);
void mpi_free(MYMPI *mpi_info);
void mpi_info_write(MYMPI *mpi_info);
void comm_swap_k_forward(double ***pmpi, double ***pkmpi, MYMPI *mi);
void comm_swap_k_backward(double ***pkmpi, double ***pmpi, MYMPI *mi);
void comm_swap_i_forward(double ***pmpi, double ***pimpi, MYMPI *mi);
void comm_swap_i_backward(double ***pimpi, double ***pmpi, MYMPI *mi);
