#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "array3d.h"
#include "realft.h"
#include "mpi_module.h"
#define PI 3.141592653589793

int main(int argc, char** argv)
{
	int nx, ny, nz;
	int nmax;
	const int npx=4, npy=1, npz=8;

	int i,j,k;
	double dx,dy,dz;
	double ***a3d, *a1d, ***a3d_k, ***a3d_i;
	MYMPI mi;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&mi.mpisize);
	MPI_Comm_rank(MPI_COMM_WORLD,&mi.mpirank);
	FILE *inputs;
	inputs = fopen(argv[argc-1],"r");
	fscanf(inputs,"%d",&nx);
	fscanf(inputs,"%d",&ny);
	fscanf(inputs,"%d",&nz);
	fscanf(inputs,"%d",&nmax);
	fclose(inputs);
	a1d=(double*)malloc(sizeof(double)*2*nmax+1);

	mpi_setup(nx, ny, nz, npx, npy, npz, &mi);
	a3d=array3D_mod(mi.nx_mpi, mi.ny, mi.nz_mpi);

	dx = 1.0/(double)mi.nx;
	dy = 1.0/(double)mi.ny;
	dz = 1.0/(double)mi.nz;

	for(i=0;i<mi.nx_mpi;i++)
		for(j=0;j<mi.ny;j++)
			for(k=0;k<mi.nz_mpi;k++)
				a3d[i][j][k]=(0.5*sin(60.0*PI*(i+mi.ista)*dx)+sin(10.0*PI*(i+mi.ista)*dx))*sin(20.0*PI*j*dy)*(1.2*sin(50.0*PI*(k+mi.ksta)*dz) + 0.1*sin(100.0*PI*(k+mi.ksta)*dz));

	a3d_k=array3D_mod(mi.nx_mpiblk, mi.ny, mi.nz);

	comm_swap_k_forward(a3d,a3d_k,&mi);

	for(i=0;i<mi.nx_mpiblk;i++) {
		for(j=0;j<mi.ny;j++) {
			for(k=0;k<mi.nz;k++) {
				a1d[k+1] = a3d_k[i][j][k];
			}
			realft(a1d,nz,1);
			for(k=0;k<mi.nz;k++) {
				a3d_k[i][j][k] = a1d[k+1]*2.0*dz;
			}
		}
	}
	comm_swap_k_backward(a3d_k,a3d,&mi);

	free_array3D_mod(a3d_k);

	for(i=0;i<mi.nx_mpi;i++) {
		for(k=0;k<mi.nz_mpi;k++) {
			for(j=0;j<ny;j++) {
				a1d[j+1] = a3d[i][j][k];
			}
			realft(a1d,ny,1);
			for(j=0;j<ny;j++) {
				a3d[i][j][k] = a1d[j+1]*2.0*dy;
			}
		}
	}

	a3d_i=array3D_mod(mi.nx, mi.ny, mi.nz_mpiblk);

	comm_swap_i_forward(a3d,a3d_i,&mi);

	for(j=0;j<ny;j++) {
		for(k=0;k<mi.nz_mpiblk;k++) {
			for(i=0;i<nx;i++) {
				a1d[i+1] = a3d_i[i][j][k];
			}
			realft(a1d,nx,1);
			for(i=0;i<nx;i++) {
				a3d_i[i][j][k] = a1d[i+1]*2.0*dx;
			}
		}
	}

	comm_swap_i_backward(a3d_i,a3d,&mi);

	free_array3D_mod(a3d_i);

	for(i=0;i<mi.nx_mpi;i++) {
		for(j=0;j<mi.ny_mpi;j++) {
			for(k=0;k<mi.nz_mpi;k++) {
				if(fabs(a3d[i][j][k]) > 1.e-6) {
					printf("Location = ( %5d, %5d, %5d ), Value = %17.10e\n",i+mi.ista,j,k+mi.ksta,a3d[i][j    ][k]);
				}
			}
		}
	}

	free(a1d);
	free_array3D_mod(a3d);

	mpi_free(&mi);
	MPI_Finalize();

	return 0;
}
