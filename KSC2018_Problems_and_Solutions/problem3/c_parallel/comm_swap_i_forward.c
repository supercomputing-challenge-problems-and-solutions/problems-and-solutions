#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include "mpi_module.h"
void comm_swap_i_forward(double ***pmpi,double ***pimpi,MYMPI *mi)
{
	double *sendbuf;
	int i,j,k,kblk;
	int kblk_block,i_block,j_block,indexbuf;

	sendbuf = (double*)malloc(mi->ntotalsize*sizeof(double));

	for(kblk=0;kblk<mi->nproc_x;kblk++) {
		kblk_block=kblk*mi->nz_mpiblk*mi->ny*mi->nx_mpi;
		for(i=0;i<mi->nx_mpi;i++) {
			i_block=i*mi->ny*mi->nz_mpiblk;
			for(j=0;j<mi->ny;j++) {
				j_block=j*mi->nz_mpiblk;
				for(k=0;k<mi->nz_mpiblk;k++) {
					indexbuf = k+j_block+i_block+kblk_block;
					sendbuf[indexbuf]=pmpi[i][j][k+kblk*mi->nz_mpiblk];
				}
			}
		}
	}

	MPI_Alltoall(sendbuf,mi->nz_blksize,MPI_DOUBLE,pimpi[0][0],mi->nz_blksize,MPI_DOUBLE,mi->MPI_COMM_X);

	free(sendbuf);
}
