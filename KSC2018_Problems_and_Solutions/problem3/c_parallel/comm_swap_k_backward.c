#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include "mpi_module.h"

void comm_swap_k_backward(double*** pimpi,double ***pmpi, MYMPI *mi)
{
	double *sendbuf;

	int i,j,k,kblk;
	int indexbuf;

	sendbuf = (double*)malloc(mi->ntotalsize*sizeof(double));

	for(kblk=0;kblk<mi->nproc_z;kblk++)
		for(i=0;i<mi->nx_mpiblk;i++)
			for(j=0;j<mi->ny;j++)
				for(k=0;k<mi->nz_mpi;k++) {
					indexbuf = k+j*mi->nz_mpi+i*mi->ny*mi->nz_mpi+kblk*mi->nx_mpiblk*mi->ny*mi->nz_mpi;
					sendbuf[indexbuf]=pimpi[i][j][k+kblk*mi->nz_mpi];
				}

	MPI_Alltoall(sendbuf,mi->nx_blksize,MPI_DOUBLE,pmpi[0][0],mi->nx_blksize,MPI_DOUBLE,mi->MPI_COMM_Z);

	free(sendbuf);

}
