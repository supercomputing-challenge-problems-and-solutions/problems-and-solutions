#include <stdio.h>
#include <stdlib.h>

double ***array3D(int nx, int ny, int nz)
{
	double ***a3d;
	int i,j;

	a3d = (double ***)malloc(sizeof(double**)*nx);

	for(i=0;i<nx;i++){
		a3d[i] = (double **)malloc(sizeof(double*)*ny);

		for(j=0;j<ny;j++){
			a3d[i][j] = (double *)malloc(sizeof(double)*nz);
		}
	}

	return a3d;
}

void free_array3D(double ***array3D, int nx, int ny, int nz)
{
	int i,j;
	for(i=0;i<nx;i++){
		for(j=0;j<ny;j++) {
			free(array3D[i][j]);
			}
		free(array3D[i]);
	}
	free(array3D);
}

double ***array3D_mod(int nx, int ny, int nz)
{
	double ***a3d;
	int i,j;

	a3d = (double ***)malloc(sizeof(double**)*nx);

	a3d[0] = (double **)malloc(sizeof(double**)*nx*ny);

	a3d[0][0] = (double *)malloc(sizeof(double**)*nx*ny*nz);

	for(i=0;i<nx;i++){
		a3d[i] = a3d[0] + i * ny;

		for(j=0;j<ny;j++){
			a3d[i][j] = a3d[0][0] + j*nz + i*nz*ny;
		}
	}

	return a3d;
}

void free_array3D_mod(double ***array3D)
{
	free(array3D[0][0]);
	free(array3D[0]);
	free(array3D);
}

