#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "mpi_module.h"

void comm_swap_k_forward(double ***pmpi, double ***pkmpi, MYMPI *mi)
{
	double *recvbuf;

	int i,j,k,kblk;
	int indexbuf;

	recvbuf = (double*)malloc(mi->ntotalsize*sizeof(double));

	MPI_Alltoall(pmpi[0][0], mi->nx_blksize,MPI_DOUBLE,recvbuf,mi->nx_blksize,MPI_DOUBLE,mi->MPI_COMM_Z);

	for(kblk=0;kblk<mi->nproc_z;kblk++)
		for(i=0;i<mi->nx_mpiblk;i++)
			for(j=0;j<mi->ny;j++)
				for(k=0;k<mi->nz_mpi;k++) {
					indexbuf = (k+j*mi->nz_mpi+i*mi->nz_mpi*mi->ny+kblk*mi->nx_mpiblk*mi->ny*mi->nz_mpi);
					pkmpi[i][j][k+kblk*mi->nz_mpi]=recvbuf[indexbuf];
				}

	free(recvbuf);
}
