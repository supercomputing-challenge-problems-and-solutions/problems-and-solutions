#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "mpi_module.h"

void comm_swap_i_backward(double ***uimpi,double ***umpi, MYMPI *mi)
{

	double *recvbuf;
	int i,j,k,kblk;
	int indexbuf;

	recvbuf = (double*)malloc(mi->ntotalsize*sizeof(double));

	MPI_Alltoall(uimpi[0][0],mi->nz_blksize,MPI_DOUBLE,recvbuf,mi->nz_blksize,MPI_DOUBLE,mi->MPI_COMM_X);

	for(kblk=0;kblk<mi->nproc_x;kblk++)
		for(i=0;i<mi->nx_mpi;i++)
			for(j=0;j<mi->ny;j++)
				for(k=0;k<mi->nz_mpiblk;k++) {
					indexbuf = k+j*mi->nz_mpiblk+i*mi->ny*mi->nz_mpiblk+kblk*mi->nz_mpiblk*mi->ny*mi->nx_mpi;
					umpi[i][j][k+kblk*mi->nz_mpiblk]=recvbuf[indexbuf];
				}

	free(recvbuf);
}
