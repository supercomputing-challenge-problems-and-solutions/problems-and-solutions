#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "array3d.h"
#include "realft.h"
#define PI 3.141592653589793

int main(int argc, char** argv)
{
	int nx, ny, nz, nmax;

	int i,j,k;
	double dx,dy,dz;
	double ***a3d, *a1d;

	FILE *inputs;
	inputs = fopen(argv[argc-1],"r");
// nmax is maximum of nx, ny, and nz
	fscanf(inputs,"%d",&nx);
	fscanf(inputs,"%d",&ny);
	fscanf(inputs,"%d",&nz);
	fscanf(inputs,"%d",&nmax);
	fclose(inputs);
	a1d=(double*)malloc(sizeof(double)*2*nmax+1);
	a3d=array3D_mod(nx, ny, nz);

	dx = 1.0/(double)nx;
	dy = 1.0/(double)ny;
	dz = 1.0/(double)nz;

// you can change the amplitute and frequency of input function for test
// but final submission should be with below input function
	for(i=0;i<nx;i++)
		for(j=0;j<ny;j++)
			for(k=0;k<nz;k++)
				a3d[i][j][k]=(0.5*sin(60.0*PI*i*dx)+sin(10.0*PI*i*dx))*sin(20.0*PI*j*dy)*(1.2*sin(50.0*PI*k*dz)+0.1*sin(100.0*PI*k*dz));

	for(i=0;i<nx;i++) {
		for(j=0;j<ny;j++) {
			for(k=0;k<nz;k++) {
				a1d[k+1] = a3d[i][j][k];
			}
			realft(a1d,nz,1);
			for(k=0;k<nz;k++) {
				a3d[i][j][k] = a1d[k+1]*2.0*dz;
			}
		}
	}

	for(i=0;i<nx;i++) {
		for(k=0;k<nz;k++) {
			for(j=0;j<ny;j++) {
				a1d[j+1] = a3d[i][j][k];
			}
			realft(a1d,ny,1);
			for(j=0;j<ny;j++) {
				a3d[i][j][k] = a1d[j+1]*2.0*dy;
			}
		}
	}

	for(j=0;j<ny;j++) {
		for(k=0;k<nz;k++) {
			for(i=0;i<nx;i++) {
				a1d[i+1] = a3d[i][j][k];
			}
			realft(a1d,nx,1);
			for(i=0;i<nx;i++) {
				a3d[i][j][k] = a1d[i+1]*2.0*dx;
			}
		}
	}

// do not change the criteria, 1.0e-6
// resulting peak location and amplitute should be matched with input frequency and input amplitute
	for(i=0;i<nx;i++) {
		for(j=0;j<ny;j++) {
			for(k=0;k<nz;k++) {
				if(fabs(a3d[i][j][k]) > 1.e-6) {
					printf("Location = ( %5d, %5d, %5d ), Value = %17.10e\n",i,j,k,a3d[i][j][k]);
				}
			}
		}
	}

	free(a1d);
	free_array3D_mod(a3d);

	return 0;
}
