
!----- Backward Alltoall communication from z-slaped DPKMPI to DPMPI
subroutine comm_swap_k_backward(dpkmpi,dpmpi)

	use mpi_module
	implicit none

	double precision :: dpmpi(1:nx_mpi,1:ny,1:nz_mpi)
	double precision :: dpkmpi(1:nx_mpi/nproc_z,1:ny,1:nz)
	double precision,allocatable :: recvbuf(:)
	integer :: i,j,k,iblk
	integer :: indexbuf

	allocate( recvbuf(nx_mpi*nz_mpi*ny) )

!----- Alltoall communication of SENDBUF to RECVBUF
	call mpi_alltoall(dpkmpi,nx_blksize,mpi_real8,recvbuf &
                   ,nx_blksize,mpi_real8,mpi_comm_z,ierr)

!----- Unpacking RECVBUF to original array, DPMPI
	do iblk=1,nproc_z
		do k=1,nz_mpi
			do j=1,ny
				do i=1,nx_mpiblk
					indexbuf = (i+(j-1)*nx_mpiblk &
								+(k-1)*nx_mpiblk*ny &
								+(iblk-1)*nx_mpiblk*ny*nz_mpi)
					dpmpi(i+(iblk-1)*nx_mpiblk,j,k) = recvbuf(indexbuf)
				enddo
			enddo
		enddo
	enddo

	deallocate(recvbuf)

	return
end
