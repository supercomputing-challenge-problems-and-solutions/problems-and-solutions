PROGRAM main
	
	use mpi_module
	implicit none

	double precision, parameter :: PI = 3.141592653589793
	integer :: nxp=2048, nyp=1024, nzp=1024
	integer :: nmax=2048
	character(len=32) :: inputs

	integer :: i,j,k
	double precision :: dx, dy, dz
	double precision, allocatable, dimension(:,:,:) :: a3d_mpi
	double precision, allocatable, dimension(:,:,:) :: a3d_i_mpi
	double precision, allocatable, dimension(:,:,:) :: a3d_k_mpi
	double precision, allocatable, dimension(:) :: a1d

	call mpi_init(ierr)
	call mpi_comm_size(MPI_COMM_WORLD,mpisize,ierr)
	call mpi_comm_rank(MPI_COMM_WORLD,mpirank,ierr)

    call get_command_argument(1,inputs)
    open(11,file=inputs,action='READ')

    read(11,100) nxp
    read(11,100) nyp
    read(11,100) nzp
    read(11,100) nmax

    close(11)

100 format(I5)

	nproc_x = 8
	nproc_y = 1
	nproc_z = 4

	call mpi_setup(nxp, nyp, nzp)

	allocate(a1d(2*nmax))
	allocate(a3d_mpi(nx_mpi,ny,nz_mpi))

	dx = 1.0/dble(nx)
	dy = 1.0/dble(ny)
	dz = 1.0/dble(nz)

	do k = 1, nz_mpi
		do j = 1, ny
			do i = 1, nx_mpi
				a3d_mpi(i,j,k) = (0.5*sin(60.0*PI*dble(ista-1+i-1)*dx) + sin(10.0*PI*dble(ista-1+i-1)*dx)) &
								* sin(20.0*PI*dble(jsta-1+j-1)*dy) &
								* (1.2*sin(50.0*PI*dble(ksta-1+k-1)*dz) + 0.1*sin(100.0*PI*dble(ksta-1+k-1)*dz) )
			enddo
		enddo
	enddo

	allocate(a3d_k_mpi(nx_mpiblk,ny,nz))

	call comm_swap_k_forward(a3d_mpi,a3d_k_mpi)
 	do j = 1, ny
		do i = 1, nx_mpiblk
 			do k = 1, nz
 				a1d(k) = a3d_k_mpi(i,j,k)
 			enddo
 			call realft(a1d,nz,1)
 			do k = 1, nz
 				a3d_k_mpi(i,j,k) = a1d(k) * (2.0*dz)
 			enddo
 		enddo
 	enddo
	call comm_swap_k_backward(a3d_k_mpi,a3d_mpi)

	deallocate(a3d_k_mpi)

	do k = 1, nz_mpi
		do i = 1, nx_mpi
		    a1d = 0.0
			do j = 1, ny
				a1d(j) = a3d_mpi(i,j,k)
			enddo
 			call realft(a1d,ny,1)
			do j = 1, ny
				a3d_mpi(i,j,k) = a1d(j) * (2.0*dy)
			enddo
		enddo
	enddo

	allocate(a3d_i_mpi(nx,ny,nz_mpiblk))

	call comm_swap_i_forward(a3d_mpi,a3d_i_mpi)

	do k = 1, nz_mpiblk
		do j = 1, ny
			do i = 1, nx
				a1d(i) = a3d_i_mpi(i,j,k)
			enddo
 			call realft(a1d,nx,1)
			do i = 1, nx
				a3d_i_mpi(i,j,k) = a1d(i) * (2.0*dx)
			enddo
		enddo
	enddo

	call comm_swap_i_backward(a3d_i_mpi,a3d_mpi)

	deallocate(a3d_i_mpi)

	do k = 1, nz_mpi
		do j = 1, ny_mpi
			do i = 1, nx_mpi
				if(abs(a3d_mpi(i,j,k))>1.0e-6) then
					write(*,101), i+ista-2,j-1,k+ksta-2,a3d_mpi(i,j,k)
				endif
			enddo
		enddo
	enddo

101 format('Location = ( ',i5,', ',i5,', 'i5,' ), Value = ',e17.10)

	deallocate(a1d)
	deallocate(a3d_mpi)

	call mpi_free
	call mpi_finalize(ierr)

END

