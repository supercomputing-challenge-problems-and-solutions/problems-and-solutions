
!----- Backward Alltoall communication from x-slaped UIMPI to UMPI
subroutine comm_swap_i_backward(uimpi,umpi)

	use mpi_module
	implicit none

	double precision ::  umpi(1:nx_mpi,1:ny,1:nz_mpi)
	double precision :: uimpi(1:nx,    1:ny,1:nz_mpi/nproc_x)
	double precision, allocatable :: sendbuf(:)
	integer :: i,j,k,iblk
	integer :: indexbuf

	nz_blksize = ntotalsize/nproc_x
	nz_mpiblk=nz_mpi/nproc_x

	allocate(sendbuf(nx_mpi*nz_mpi*(ny)))

!----- Packing x-slapped array, UIMPI element to SENDBUF
	do iblk=1,nproc_x
		do k=1,nz_mpiblk
			do j=1,ny
				do i=1,nx_mpi
					indexbuf = i+(j-1)*nx_mpi+(k-1)*ny*nx_mpi &
				                +(iblk-1)*nz_mpiblk*ny*nx_mpi
					sendbuf(indexbuf)=uimpi(i+(iblk-1)*nx_mpi,j,k)
				enddo
			enddo
		enddo
	enddo

!----- Alltoall communication of SENDBUF to RECVBUF
	call mpi_alltoall(sendbuf,nz_blksize,mpi_real8,umpi &
                   ,nz_blksize,mpi_real8,mpi_comm_x,ierr)

	deallocate(sendbuf)

	return
end
