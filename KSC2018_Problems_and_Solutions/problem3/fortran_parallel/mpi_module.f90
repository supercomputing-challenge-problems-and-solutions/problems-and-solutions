module mpi_module

	implicit none
      
	include 'mpif.h'

	integer :: nproc_x, nproc_y, nproc_z
	integer :: nx, ny, nz
	integer :: nx_mpi, ny_mpi, nz_mpi
	integer :: mpirank,mpisize
	integer :: mpi_comm_x,mpi_comm_z
	integer :: mpirank_x,mpirank_z
	integer :: mpisize_x,mpisize_z
	integer :: idx,idy,idz
	integer :: ista,iend,jsta,jend,ksta,kend
	integer :: ierr
	integer :: ntotalsize,nz_blksize,nx_blksize,nz_mpiblk,nx_mpiblk


contains
!----- Setup parameters for MPI
	subroutine mpi_setup(nxp,nyp,nzp)

	implicit none
	include 'mpif.h'

	integer :: nxp, nyp, nzp
	integer :: i

	nx = nxp
	ny = nyp
	nz = nzp

	nx_mpi = nx / nproc_x
	ny_mpi = ny / nproc_y
	nz_mpi = nz / nproc_z

	ntotalsize = nx_mpi*nz_mpi*ny
	nz_blksize = ntotalsize/nproc_x
	nx_blksize = ntotalsize/nproc_z
	nz_mpiblk=nz_mpi/nproc_x
	nx_mpiblk=nx_mpi/nproc_z

	idx = int(mpirank/nproc_z)
	idy = 0
	idz = mpirank - idx*nproc_z

	ista=nx/nproc_x*idx+1
	iend=ista+nx_mpi-1
	jsta=ny/nproc_y*idy+1
	jend=jsta+ny_mpi-1
	ksta=nz/nproc_z*idz+1
	kend=ksta+nz_mpi-1

	call mpi_comm_split(mpi_comm_world,idz,idx,mpi_comm_x,ierr)
	call mpi_comm_split(mpi_comm_world,idx,idz,mpi_comm_z,ierr)

	call mpi_comm_size(mpi_comm_x,mpisize_x,ierr)
	call mpi_comm_rank(mpi_comm_x,mpirank_x,ierr)

	call mpi_comm_size(mpi_comm_z,mpisize_z,ierr)
	call mpi_comm_rank(mpi_comm_z,mpirank_z,ierr)
 
    return
	end subroutine mpi_setup

	subroutine mpi_info_write

	integer :: i
	call mpi_barrier(mpi_comm_world,ierr)
	do i=1,mpisize
        if(mpirank.eq.i-1) then
          write(*,101) mpirank,'my mpi id =',idx,idy,idz
          write(*,101) mpirank,'mpi size  =',nx_mpi,ny_mpi,nz_mpi
          write(*,103) mpirank,'ista,iend =',ista,iend
          write(*,103) mpirank,'jsta,jend =',jsta,jend
          write(*,103) mpirank,'ksta,kend =',ksta,kend
          write(*,103) mpirank,'x/z rank  =',mpirank_x,mpirank_z
          write(*,103) mpirank,'x/z size  =',mpisize_x,mpisize_z
        endif
        call mpi_barrier(mpi_comm_world,ierr)
    enddo
    call mpi_barrier(mpi_comm_world,ierr)

101   format(i5,x,a11,x,'(',i4,' ,',i4,' ,',i4,' )')
102   format(i5,x,a11,x,'(',i4,' )')
103   format(i5,x,a11,x,'(',i4,' ,',i4,' )')

	return
	end subroutine mpi_info_write

	subroutine mpi_free

	call mpi_comm_free(mpi_comm_z,ierr)
	call mpi_comm_free(mpi_comm_x,ierr)

	end subroutine mpi_free

end module mpi_module
