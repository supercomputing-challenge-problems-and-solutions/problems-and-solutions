#!/bin/bash
#$ -V
#$ -cwd
#$ -N openmpi_job
#$ -pe mpi_fu 32
#$ -q ksc2018@tachyon0448,ksc2018@tachyon0449,ksc2018@tachyon0451,ksc2018@tachyon0452
#$ -R yes
#$ -l h_rt=01:00:00


# If you want to run job on more than 128 nodes, then use "plm_rsh_num_concurrent [number]" parameter
# where [number] means number of nodes on which the job will be run.
MCAArgs="-mca btl self,openib -mca plm_rsh_num_concurrent 400" 
MCAArgs="$MCAArgs -mca oob_tcp_listen_mode listen_thread"
#MCAArgs="$MCAArgs -mca plm_rsh_tree_spawn 1"

time mpirun $MCAArgs -np $NSLOTS ./main.e grid1.dat
time mpirun $MCAArgs -np $NSLOTS ./main.e grid2.dat
exit 0
