
!----- Forkward Alltoall communication from org. DPMPI to z-slaped DPKMPI
subroutine comm_swap_k_forward(dpmpi,dpkmpi)

	use mpi_module
	implicit none

	double precision :: dpmpi(1:nx_mpi,1:ny,1:nz_mpi)
	double precision :: dpkmpi(1:nx_mpi/nproc_z,1:ny,1:nz)
	double precision,allocatable :: sendbuf(:)
	integer :: i,j,k,iblk
	integer :: indexbuf

	allocate( sendbuf(nx_mpi*nz_mpi*ny) )

!----- Packing original array, DPMPI to SENDBUF
	do iblk=1,nproc_z
		do k=1,nz_mpi
			do j=1,ny
				do i=1,nx_mpiblk
					indexbuf = (i+(j-1)*nx_mpiblk &
				               +(k-1)*nx_mpiblk*ny &
				               +(iblk-1)*nx_mpiblk*ny*nz_mpi)
					sendbuf(indexbuf)=dpmpi(i+(iblk-1)*nx_mpiblk,j,k)
				enddo
			enddo
		enddo
	enddo

!----- Alltoall communication of SENDBUF to RECVBUF
	call mpi_alltoall(sendbuf,nx_blksize,mpi_real8,dpkmpi &
                   ,nx_blksize,mpi_real8,mpi_comm_z,ierr)

	deallocate(sendbuf)

	return
end
