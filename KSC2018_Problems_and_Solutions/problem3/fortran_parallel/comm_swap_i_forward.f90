
!----- Forkward Alltoall communication from org. UMPI to x-slaped UIMPI
subroutine comm_swap_i_forward(pmpi,uimpi)
	use mpi_module
	implicit none

	double precision :: pmpi (1:nx_mpi,1:ny,1:nz_mpi)
	double precision :: uimpi(1:nx,    1:ny,1:nz_mpi/nproc_x)
	double precision,allocatable :: recvbuf(:)
	integer :: i,j,k,iblk
	integer :: iblk_block,k_block,j_block,indexbuf

	allocate(recvbuf(nx_mpi*nz_mpi*ny))

!----- Alltoall communication of SENDBUF to RECVBUF
	call mpi_alltoall(pmpi,nz_blksize,mpi_real8,recvbuf &
					,nz_blksize,mpi_real8,mpi_comm_x,ierr)

!----- Unpacking RECVBUF to x-slapped array, UIMPI
	do iblk=1,nproc_x
		iblk_block=(iblk-1)*(nz_mpiblk)*ny*nx_mpi
		do k=1,nz_mpiblk
			k_block=(k-1)*ny*nx_mpi
			do j=1,ny
				j_block=(j-1)*nx_mpi
				do i=1,nx_mpi
					indexbuf = i+j_block+k_block+iblk_block
					uimpi(i+(iblk-1)*nx_mpi,j,k) = recvbuf(indexbuf)
				enddo
			enddo
		enddo
	enddo

	deallocate(recvbuf)

	return
end
