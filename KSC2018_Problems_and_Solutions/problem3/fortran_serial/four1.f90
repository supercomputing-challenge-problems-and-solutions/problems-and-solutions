SUBROUTINE four1(data,nn,isign) 
	implicit none 
	double precision :: data(2*nn)
	integer :: isign,nn

	double precision, parameter :: PI = 3.141592653589793
	double precision :: tempi,tempr
	double precision :: theta,wi,wpi,wpr,wr,wtemp
	integer :: i,istep,j,m,mmax,n

	n=2*nn 
	j=1

	do i=1,n,2 
		if(j.gt.i) then
			tempr=data(j) 
			tempi=data(j+1) 
			data(j)=data(i) 
			data(j+1)=data(i+1) 
			data(i)=tempr 
			data(i+1)=tempi
		endif
		m=n/2
		do while((m.ge.2).and.(j.gt.m))
			j=j-m
			m=m/2 
		enddo
		j=j+m 
	enddo

	mmax=2

!2	if (n.gt.mmax) then
	do while(n.gt.mmax)
		istep=2*mmax
		theta=2.0*PI/(isign*mmax) 
		wpr=-2.d0*sin(0.5d0*theta)**2
		wpi=sin(theta)
		wr=1.d0
		wi=0.d0
	
		do m=1,mmax,2
			do i=m,n,istep
				j=i+mmax 
				tempr=wr*data(j)-wi*data(j+1) 
				tempi=wr*data(j+1)+wi*data(j) 
				data(j)=data(i)-tempr
				data(j+1)=data(i+1)-tempi
				data(i)=data(i)+tempr
				data(i+1)=data(i+1)+tempi
			enddo
			wtemp=wr
			wr=wr*wpr-wi*wpi+wr
			wi=wi*wpr+wtemp*wpi+wi
		enddo
		mmax=istep 
	enddo
!		goto 2
!	endif
	return
END
