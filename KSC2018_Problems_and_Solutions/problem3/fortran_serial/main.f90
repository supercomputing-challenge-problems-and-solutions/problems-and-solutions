PROGRAM main
implicit none

	double precision, parameter :: PI = 3.141592653589793

	integer :: nx, ny, nz
	integer :: nmax
	character(len=32) :: inputs

	integer :: i,j,k
	double precision :: dx, dy, dz
	double precision, allocatable, dimension(:,:,:) :: a3d
	double precision, allocatable, dimension(:) :: a1d

	call get_command_argument(1,inputs)

	open(11,file=inputs,action='READ')

! nmax is maximum of nx, ny, and nz
	read(11,*) nx
	read(11,*) ny
	read(11,*) nz
	read(11,*) nmax

	close(11)

	allocate(a1d(2*nmax))
	allocate(a3d(nx,ny,nz))

	dx = 1.0/dble(nx)
	dy = 1.0/dble(ny)
	dz = 1.0/dble(nz)

! you can change the amplitute and frequency of input function for test
! but final submission should be with below input function
	do k = 1, nz
		do j = 1, ny
			do i = 1, nx
				a3d(i,j,k) = (0.5*sin(60.0*PI*dble(i-1)*dx)+sin(10.0*PI*dble(i-1)*dx)) &
				           * sin(20.0*PI*dble(j-1)*dy) &
				           * (1.2*sin(50.0*PI*dble(k-1)*dz)+0.1*sin(100.0*PI*dble(k-1)*dz) )
			enddo
		enddo
	enddo

 	do j = 1, ny
		do i = 1, nx
 			do k = 1, nz
 				a1d(k) = a3d(i,j,k)
 			enddo
 			call realft(a1d,nz,1)
 			do k = 1, nz
 				a3d(i,j,k) = a1d(k) * (2.0*dz)
 			enddo
 		enddo
 	enddo

	do k = 1, nz
		do i = 1, nx
			do j = 1, ny
				a1d(j) = a3d(i,j,k)
			enddo
 			call realft(a1d,ny,1)
			do j = 1, ny
				a3d(i,j,k) = a1d(j) * (2.0*dy)
			enddo
		enddo
	enddo

	do k = 1, nz
		do j = 1, ny
			do i = 1, nx
				a1d(i) = a3d(i,j,k)
			enddo
 			call realft(a1d,nx,1)
			do i = 1, nx
				a3d(i,j,k) = a1d(i) * (2.0*dx)
			enddo
		enddo
	enddo

! do not change the criteria, 1.0e-6
! resulting peak location and amplitute should be matched with input frequency and input amplitute
	do k = 1, nz
		do j = 1, ny
			do i = 1, nx
				if(abs(a3d(i,j,k))>1.0e-6) then
					write(*,101), i-1,j-1,k-1,a3d(i,j,k)
				endif
			enddo
		enddo
	enddo
101 format('Location = ( ',i5,', ',i5,', 'i5,' ), Value = ',e17.10)

	deallocate(a1d)
	deallocate(a3d)

END

