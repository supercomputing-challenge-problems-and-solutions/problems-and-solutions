program randomwalk
    implicit none
    integer :: i,j,rtc
    integer, parameter :: N_P=1000000, N_W=1000
    integer, dimension(N_P) :: part_x, part_y
    integer, dimension(:,:),allocatable :: pos
    real :: seed, rand,temp
    part_x=0
    part_y=0
    call srand(0.5)
    do i=1, N_P
        do j=1,N_W
            temp=rand(0)
            if(temp <= 0.25) then
                part_x(i)=part_x(i)+1
            elseif(temp <= 0.5) then
                part_y(i)=part_y(i)+1
            elseif(temp <=0.75) then
                part_x(i)=part_x(i)-1
            else
                part_y(i)=part_y(i)-1
            endif
        enddo
    enddo
    allocate(pos(-N_W:N_W,-N_W:N_W))
    pos=0
    do i=1,N_P
        pos(part_x(i),part_y(i))=pos(part_x(i),part_y(i))+1
    enddo
    do i=-N_W,N_W,2
        do j=-N_W,N_W,2
            if(pos(i,j) .ne. 0) print*, i,j, pos(i,j)
        enddo
    enddo
    deallocate(pos)
end program randomwalk