program randomwalk
    implicit none
    include 'mpif.h'
    integer :: mysize, myrank, ierr, pos_count, ista, iend
    integer :: i,j,d,min_p,max_p,rtc
    integer, parameter :: N_P=1000000, N_W=1000
    integer, dimension(N_P) :: part_x, part_y
    integer, dimension(:,:),allocatable :: pos,pos_all
    real :: seed, rand,temp
    part_x=0
    part_y=0
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,mysize, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank, ierr)
    call srand(0.5+myrank*0.1)
    call para_range(1,N_P,mysize,myrank,ista,iend)
    do i=ista,iend
        do j=1,N_W
            temp=rand(0)
            if(temp <= 0.25) then
                part_x(i)=part_x(i)+1
            elseif(temp <= 0.5) then
                part_y(i)=part_y(i)+1
            elseif(temp <=0.75) then
                part_x(i)=part_x(i)-1
            else
                part_y(i)=part_y(i)-1
            endif
        enddo
    enddo
    allocate(pos(-N_W:N_W,-N_W:N_W))
    allocate(pos_all(-N_W:N_W,-N_W:N_W))
    pos=0
    do i=1,N_P
        pos(part_x(i),part_y(i))=pos(part_x(i),part_y(i))+1
    enddo
    pos_count=size(pos)
    call MPI_REDUCE(pos,pos_all,pos_count,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ierr)
    if(myrank==0) then
        pos=pos_all
        do i=-N_W,N_W,2
            do j=-N_W,N_W,2
                if(pos(i,j) .ne. 0) print*, i,j, pos(i,j)
            enddo
        enddo
    endif
    deallocate(pos)
    deallocate(pos_all)
    call MPI_FINALIZE(ierr)

CONTAINS
    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
        implicit none
        integer :: n1,n2,nprocs,irank,ista,iend
        integer :: iwork1,iwork2
        iwork1 = (n2 - n1 + 1) / nprocs
        iwork2 = mod(n2 - n1 + 1, nprocs)
        ista = irank * iwork1 + n1 + min(irank,iwork2)
        iend = ista + iwork1 -1
        if(iwork2 .gt. irank) iend = iend + 1
        return
    end subroutine
end program randomwalk