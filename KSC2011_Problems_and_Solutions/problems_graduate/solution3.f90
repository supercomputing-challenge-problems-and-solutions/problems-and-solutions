PROGRAM FDM2D
    IMPLICIT NONE
    INCLUDE 'mpif.h'
    INTEGER :: nprocs, myrank, viewrank, ista, iend, jsta, jend, ierr, status(MPI_STATUS_SIZE), inext,
    iprev, isend1, isend2, irecv1, irecv2
    INTEGER :: i, j, k
    INTEGER :: im,jm,im1,jm1, njm
    PARAMETER(im=300,jm=300)
    INTEGER :: is,ie,js,je
    INTEGER :: iter,itermax,nprt
    REAL :: tolerance
    REAL :: bc(4) !left,right,bottom,top
    REAL :: u(0:im+1,0:jm+1), newu(0:im+1, 0:jm+1), works1( jm), works2( jm), workr1( jm),
    workr2( jm)
    REAL :: error, errorsum
    CHARACTER*2 :: number
    CHARACTER*30 :: filename(0:10)
    INTEGER :: irecv(0:3), iscnt, ircnt(0:3), idisp(0:3)=(/1,33,65,97/)
    viewrank = 3
    ! mpi ready
    CALL MPI_INIT(ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)
    CALL MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierr)
    CALL para_range(1,im, nprocs, myrank, ista, iend)
    ! read input data
    itermax=100000
    nprt=500
    tolerance = 1.0e-7
    bc(1) = 10.0
    bc(2) = 10.0
    bc(3) = 10.0
    bc(4) = 20.0
    ! initialize
    im1=im+1
    jm1=jm+1
    DO j=0,jm1
        DO i=0,im1
            u(i,j) = 0.0
        END DO
    END DO
    ! boundary conditions
    IF(myrank == 0) THEN
        DO j=0, jm+1
            u(0 ,j) = bc(1) !left
        ENDDO
    ENDIF
    IF(myrank == nprocs-1) THEN
        DO j=0, jm+1
            u(im1,j) = bc(2) !right
        END DO
    ENDIF
    DO i=ista, iend
        u(i,0 ) = bc(3) !bottom
        u(i,jm1) = bc(4) !top
    END DO
    ! set computation range
    is = ista
    ie = iend
    js = 1
    je = jm
    inext = myrank + 1
    iprev = myrank - 1
    IF(myrank == 0) iprev = MPI_PROC_NULL
    IF(myrank == nprocs -1 ) inext = MPI_PROC_NULL
    ! main routine
    iter = 0
    error = 1000.
    errorsum = 1000.
    ! *********************************************************************
    DO WHILE(iter.LE.itermax.AND.errorsum.GT.tolerance)
        ! do while(iter.le.itermax)
        IF(myrank /= nprocs-1) THEN
            DO j=1, jm
                works1( j) = u(iend, j)
            ENDDO
        ENDIF
        IF(myrank /= 0) THEN
            DO j=1, jm
                works2( j) = u(ista, j)
            ENDDO
        ENDIF
        CALL MPI_ISEND(works1, jm, MPI_REAL, inext, 1, MPI_COMM_WORLD, isend1, ierr)
        CALL MPI_ISEND(works2, jm, MPI_REAL, iprev, 1, MPI_COMM_WORLD, isend2, ierr)
        CALL MPI_IRECV(workr1, jm, MPI_REAL, iprev, 1, MPI_COMM_WORLD, irecv1, ierr)
        CALL MPI_IRECV(workr2, jm, MPI_REAL, inext, 1, MPI_COMM_WORLD, irecv2, ierr)
        CALL MPI_WAIT(isend1, status, ierr)
        CALL MPI_WAIT(isend2, status, ierr)
        CALL MPI_WAIT(irecv1, status, ierr)
        CALL MPI_WAIT(irecv2, status, ierr)
        IF(myrank /= 0)THEN
            DO j=1, jm
                u(ista-1, j)=workr1( j)
            ENDDO
        ENDIF
        IF(myrank /= nprocs-1) THEN
            DO j=1, jm
                u(iend+1, j)= workr2( j)
            ENDDO
        ENDIF
        call jacobi(u,im,jm,is,ie,js,je,error)
        iter = iter + 1
        CALL MPI_ALLREDUCE(error, errorsum, 1, MPI_REAL, MPI_SUM, MPI_COMM_WORLD, ierr)
        if((mod(iter,nprt).eq.0) .and. myrank == viewrank) write(*,100) iter,errorsum
    end do
    ! *********************************************************************
    100 format('Iteration=',i6,' Error=',e9.4)
    IF(myrank == 0) THEN
        print*,'Error=',errorsum
        print*,'Converged after ',iter,'iteration'
        200 format('Iteration=',i6,' Error=',e9.4)
    ENDIF
    ! delete communication data
    IF(myrank /= 0)THEN
        DO j=1, jm
            u(ista-1, j)=0.0
        ENDDO
    ENDIF
    IF(myrank /= nprocs-1) THEN
        DO j=1, jm
            u(iend+1, j)=0.0
        ENDDO
    ENDIF
    CALL MPI_REDUCE(u, newu, (im+2)*( jm+2), MPI_REAL, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
    CALL MPI_FINALIZE(ierr)
    STOP
CONTAINS
    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
        implicit none
        integer :: n1,n2,nprocs,irank,ista,iend
        integer :: iwork1,iwork2
        iwork1 = (n2 - n1 + 1) / nprocs
        iwork2 = mod(n2 - n1 + 1, nprocs)
        ista = irank * iwork1 + n1 + min(irank,iwork2)
        iend = ista + iwork1 -1
        if(iwork2 .gt. irank) iend = iend + 1
        return
    end subroutine

    ! jacobi subroutine
    subroutine jacobi(u,im,jm,is,ie,js,je,error)
        integer im,jm,is,ie,js,je,njm
        integer i,j
        real error
        real u(0:im+1,0:jm+1), uo(0:im+1,0:jm+1)
        do j=js-1,je+1
            do i=is-1,ie+1
                uo(i,j) = u(i,j)
            end do
        end do
        ! jacobi
        do j=js,je
            do i=is,ie
                u(i,j) = (uo(i-1,j)+uo(i+1,j)+uo(i,j-1)+uo(i,j+1))/4.0
            end do
        end do
        ! error
        error = 0.0
        do j=js,je
            do i=is,ie
                error = error + (u(i,j) - uo(i,j))**2
            end do
        end do
        return
    end subroutine
end program FDM2D