program md
    implicit none
    integer :: i,j,k
    integer, parameter :: N_P=1000, step=10000
    real, dimension(N_P) :: x, force
    real :: f_jk
    do i=1,N_P
    read *, x(i)
    x(i)=x(i)*10000000.0
    enddo
    do i=1,step
        force=0.0
        do j=1,N_P-1
            do k= j+1,N_P
                f_jk = 1.0/(x(k)-x( j))
                force( j)=force( j)+f_jk
                force(k)=force(k)-f_jk
            enddo
        enddo
        do j=1,N_P
            x( j)=x( j)+force( j)
        enddo
    enddo
    do i=1,N_P
        print *, i,x(i)
    enddo
end program md