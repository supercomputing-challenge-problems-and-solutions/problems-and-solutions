program FDM2D
    integer im,jm,im1,jm1
    parameter(im=300,jm=300)
    integer is,ie,js,je
    integer iter,itermax,nprt
    real tolerance
    real bc(4) !left,right,bottom,top
    real u(0:im+1,0:jm+1)
    real error
    ! read input data
    itermax=100000
    nprt=500
    tolerance = 1.0e-7
    bc(1) = 10.0
    bc(2) = 10.0
    bc(3) = 10.0
    bc(4) = 20.0
    ! initialize
    im1=im+1
    jm1=jm+1
    do j=0,jm1
        do i=0,im1
            u(i,j) = 0.0
        end do
    end do
    ! boundary conditions
    do j=0,jm1
        u(0 ,j) = bc(1) !left
        u(im1,j) = bc(2) !right
    end do
    do i=0,im1
        u(i,0 ) = bc(3) !bottom
        u(i,jm1) = bc(4) !top
    end do
    ! set computation range
    is = 1
    ie = im
    js = 1
    je = jm
    ! main routine
    iter = 0
    error = 1000.
    ! *********************************************************************
    do while(iter.le.itermax.and.error.gt.tolerance)
        call jacobi(u,im,jm,is,ie,js,je,error)
        iter = iter + 1
        if(mod(iter,nprt).eq.0) write(*,100) iter,error
    end do
    ! *********************************************************************
    print*,'Error=',error
    print*,'Converged after ',iter,'iteration'
    100 format('Iteration=',i6,' Error=',e9.4)
    ! OPEN(UNIT=10, FILE='outFDM2Seq.dat', STATUS='REPLACE', ACTION='WRITE')
    DO j=1,jm
    ! WRITE (*, '128(F7.4,1x)'), (u(i,j), i=1,im)
    ! WRITE (10, '128(F7.4,1x)'), (u(i,j), i=1,im)
    ENDDO
    ! CLOSE(10)
    stop
end

subroutine jacobi(u,im,jm,is,ie,js,je,error)
    integer im,jm,is,ie,js,je
    integer i,j
    real error
    real u(0:im+1,0:jm+1), uo(0:im+1,0:jm+1)
    ! store old data
    do j=0,jm+1
       do i=0,im+1
            uo(i,j) = u(i,j)
        end do
    end do
    ! jacobi
    do j=js,je
        do i=is,ie
            u(i,j) = (uo(i-1,j)+uo(i+1,j)+uo(i,j-1)+uo(i,j+1))/4.0
        end do
    end do
    ! error
    error = 0.0
    do j=js,je
        do i=is,ie
            error = error + (u(i,j) - uo(i,j))**2
        end do
    end do
    return
end