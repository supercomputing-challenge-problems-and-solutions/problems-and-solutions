program md
    implicit none
    include 'mpif.h'
    integer :: mysize, myrank, ierr, ista, iend
    integer :: i,j,k
    integer, parameter :: N_P=1000, step=10000
    real, dimension(N_P) :: x, force, force_all
    real :: f_jk
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,mysize, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank, ierr)
    if(myrank==0) then
        do i=1,N_P
            read *, x(i)
            x(i)=x(i)*10000000.0
        enddo
        print *,'start'
    endif
    call MPI_BCAST(x,N_P,MPI_REAL,0,MPI_COMM_WORLD,ierr)
    do i=1,step
        force=0.0
        call para_range(1,N_P-1,mysize,myrank,ista,iend)
        do j=ista,iend
            do k= j+1,N_P
                f_jk = 1.0/(x(k)-x( j))
                force( j)=force( j)+f_jk
                force(k)=force(k)-f_jk
            enddo
        enddo
        call MPI_REDUCE(force,force_all,N_P,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)
        if(myrank==0) then
            do j=1,N_P
                x( j)=x( j)+force_all( j)
            enddo
        endif
    enddo
    if(myrank==0) then
        do i=1,N_P
            print *, i,x(i)
        enddo
    endif
    call MPI_FINALIZE(ierr)
CONTAINS
    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
        implicit none
        integer :: n1,n2,nprocs,irank,ista,iend
        integer :: iwork1,iwork2
        iwork1 = (n2 - n1 + 1) / nprocs
        iwork2 = mod(n2 - n1 + 1, nprocs)
        ista = irank * iwork1 + n1 + min(irank,iwork2)
        iend = ista + iwork1 -1
        if(iwork2 .gt. irank) iend = iend + 1
        return
    end subroutine
end program md