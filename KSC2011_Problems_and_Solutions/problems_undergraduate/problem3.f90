program taylor
    implicit none
    real(8) :: x, mysin, tmp, x_max, step
    integer(4) :: i, j, k, n, num_step
    x_max=4.0d0
    x=0.0d0
    num_step=10000
    n=1000
    step=x_max/real(num_step,8)
    do i=0,num_step
        mysin=0.0d0
        do j=1,n
            tmp=1.0d0
            do k=1,2*j-1
                tmp=tmp*x/real(k,8)
            enddo
            if(mod( j,2)) then
                mysin=mysin+tmp
            else
                mysin=mysin-tmp
            endif
        enddo
        print *, x, mysin, sin(x)
        x=x+step
    enddo
end program taylor