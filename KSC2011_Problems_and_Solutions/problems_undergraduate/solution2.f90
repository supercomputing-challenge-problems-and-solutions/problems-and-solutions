program matrix
    implicit none
    include 'mpif.h'
    integer :: i,j,k,n,is,ie
    integer(4) :: myrank, nprocs, ierr
    real,dimension(:,:),allocatable :: a,b,c
    real :: sum,psum
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
    n=5000
    sum=0.0
    psum=0.0
    allocate (a(n,n))
    allocate (b(n,n))
    allocate (c(n,n))
    c=0.0
    is=1;ie=n
    call para_range(is,ie,nprocs,myrank,is,ie)
    do j=1,n
        do i=1,n
            a(i,j)=real(i)/real( j)
            b(i,j)=real( j)/real(i)
        end do
    end do
    do k=1,n
        do j=is,ie
            do i=1,n
                c(i,j)=c(i,j)+a(i,k)*b(k,j)
            end do
        end do
    end do
    do j=is,ie
        do i=1,n
            psum=psum+c(i,j)
        enddo
    enddo
    call MPI_REDUCE(psum,sum,1,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)
    if(myrank.eq.0) then
        print *, 'SUM = ',sum
    endif
    deallocate(a,b,c)
    call MPI_FINALIZE(ierr)
    stop

    contains

    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
        implicit none
        integer :: nprocs,irank
        integer :: np,ir
        integer :: n1,n2,ista,iend
        integer :: iwork1,iwork2
        np=nprocs
        ir=irank
        iwork1 = (n2 - n1 + 1) / np
        iwork2 = mod(n2 - n1 + 1, np)
        ista = ir * iwork1 + n1 + min(ir,iwork2)
        iend = ista + iwork1 -1
        if(iwork2 .gt. ir) iend = iend + 1
        return
    end subroutine
end program matrix