program integration
    implicit none
    include 'mpif.h'
    real(8) :: pi, x, x_max, sum
    integer(8) :: num_step, i, is, ie
    integer(4) :: myrank, nprocs, ierr
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
    sum=0.0d0
    x=0.0d0
    x_max=100000.0d0
    num_step=5000000000
    is = 1 ; ie = num_step
    call para_range(is,ie,nprocs,myrank,is,ie)
    do i = is, ie
        x = x_max*real(i,8)/real(num_step,8)
        sum = sum + 2.0d0*sin(x)/x*x_max/real(num_step,8)
    end do
    call MPI_REDUCE(sum,pi,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ierr)
    if(myrank.eq.0) then
        print *, 'PI = ',pi
    endif
    call MPI_FINALIZE(ierr)
    stop

contains

    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
        implicit none
        integer :: nprocs,irank
        integer(8) :: np,ir
        integer(8) :: n1,n2,ista,iend
        integer(8) :: iwork1,iwork2
        np=int(nprocs,8)
        ir=int(irank,8)
        iwork1 = (n2 - n1 + 1) / np
        iwork2 = mod(n2 - n1 + 1, np)
        ista = ir * iwork1 + n1 + min(ir,iwork2)
        iend = ista + iwork1 -1
        if(iwork2 .gt. ir) iend = iend + 1
        return
    end subroutine
    
end program integration