program taylor
    implicit none
    include 'mpif.h'
    real(8) :: x, mysin, tmp, x_max, step
    integer(4) :: i, j, k, n, num_step
    integer(4) :: myrank, nprocs, ierr, is, ie
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
    x_max=4.0d0
    num_step=10000
    n=1000
    step=x_max/real(num_step,8)
    is=0;ie=num_step
    call para_range(is,ie,nprocs,myrank,is,ie)
    do i=is,ie
        x=real(i,8)*step
        mysin=0.0d0
        do j=1,n
            tmp=1.0d0
            do k=1,2*j-1
                tmp=tmp*x/real(k,8)
            enddo
            if(mod( j,2)) then
                mysin=mysin+tmp
            else
                mysin=mysin-tmp
            endif
        enddo
        print *, x, mysin, sin(x)
    enddo
    call MPI_FINALIZE(ierr)

contains

    subroutine para_range(n1,n2,nprocs,irank,ista,iend)
    implicit none
    integer :: nprocs,irank
    integer :: np,ir
    integer :: n1,n2,ista,iend
    integer :: iwork1,iwork2
    np=nprocs
    ir=irank
    iwork1 = (n2 - n1 + 1) / np
    iwork2 = mod(n2 - n1 + 1, np)
    ista = ir * iwork1 + n1 + min(ir,iwork2)
    iend = ista + iwork1 -1
    if(iwork2 .gt. ir) iend = iend + 1
    return
    end subroutine
end program taylor