program matrix
    implicit none
    integer :: i,j,k,n
    real,dimension(:,:),allocatable :: a,b,c
    real :: sum
    n=5000
    sum=0.0
    allocate (a(n,n))
    allocate (b(n,n))
    allocate (c(n,n))
    c=0.0
    do j=1,n
        do i=1,n
            a(i,j)=real(i)/real( j)
            b(i,j)=real( j)/real(i)
        end do
    end do
    do k=1,n
        do j=1,n
            do i=1,n
                c(i,j)=c(i,j)+a(i,k)*b(k,j)
            end do
        end do
    end do
    do j=1,n
        do i=1,n
            sum=sum+c(i,j)
        enddo
    enddo
    print *, 'SUM = ',sum
    deallocate(a,b,c)
    end program matrix