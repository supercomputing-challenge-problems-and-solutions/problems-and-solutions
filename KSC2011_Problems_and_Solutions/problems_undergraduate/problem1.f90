program integration
    implicit none
    real(8) :: pi, x, x_max
    integer(8) :: num_step, i
    pi=0.0d0
    x=0.0d0
    x_max=100000.0d0
    num_step=5000000000
    do i = 1, num_step
        x = x_max*real(i,8)/real(num_step,8)
        pi = pi + 2.0d0*sin(x)/x*x_max/real(num_step,8)
    end do
    print *,'PI = ',pi
end program integration