program FDM2D
    integer im,jm,im1,jm1
    parameter(im=300,jm=300)
    integer is,ie,js,je
    integer iter,itermax,nprt
    real*8 tolerance , time_start, time_end, elapsed_time
    real*8 bc(4) !left,right,bottom,top
    real*8 u(0:im+1,0:jm+1)
    real*8 error
    ! read input data
    itermax=100000
    nprt=500
    tolerance = 1.0d-7
    bc(1) = 10.0
    bc(2) = 10.0
    bc(3) = 10.0
    bc(4) = 20.0
    ! initialize
    im1=im+1
    jm1=jm+1
    do j=0,jm1
        do i=0,im1
            u(i,j) = 0.0
        end do
    end do
    ! boundary conditions
    do j=0,jm1
        u(0 ,j) = bc(1) !left
        u(im1,j) = bc(2) !right
    end do
    do i=0,im1
        u(i,0 ) = bc(3) !bottom
        u(i,jm1) = bc(4) !top
    end do
    ! set computation range
    is = 1
    ie = im
    js = 1
    je = jm
    ! main routine
    iter = 0
    error = 1000.
    ! ******************************************************************
    call cpu_time(time_start)
    do while(iter.le.itermax.and.error.gt.tolerance)
        call jacobi(u,im,jm,is,ie,js,je,error)
        iter = iter + 1
    end do
    call cpu_time(time_end)
    ! *****************************************************************
    print*,'Error=',error
    print*,'Converged after ',iter,'iteration'
    100 format('Iteration=',i6,' Error=',e9.4)
    elapsed_time = time_end - time_start
    print*,'elapsed time=',elapsed_time
    print FLOPS between time check routine
    stop
end

subroutine jacobi(u,im,jm,is,ie,js,je,error)
    integer im,jm,is,ie,js,je
    integer i,j
    real error
    real u(0:im+1,0:jm+1), uo(0:im+1,0:jm+1)
    ! store old data
    do j=0,jm+1
        do i=0,im+1
            uo(i,j) = u(i,j)
        end do
    end do
    ! jacobi
    do j=js,je
        do i=is,ie
            u(i,j) = (u(i-1,j)+uo(i+1,j)+u(i,j-1)+uo(i,j+1))/4.0
        end do
    end do
    ! error
    error = 0.0
    do j=js,je
        do i=is,ie
            error = error + (u(i,j) - uo(i,j))**2
        end do
    end do
    return
end