#include <stdio.h>
#include <math.h>
#define im 100
#define jm 100
#define im1 101
#define jm1 101
#define ITER_MAX 100000
int main()
{
    int i,j,iter, nprt;
    double tolerance, error;
    double u[im1+1][jm1+1], uo[im1+1][jm1+1];
    double elapsed_time;
    struct timeval tv_start, tv_end, tv_diff;
    // Read Input
    iter=0;
    nprt = 500;
    tolerance = 1.e-7 ;
    elapsed_time=0.0;
    // initialize
    for (i=0; i<=im1; i++) {
        for (j=0; j<=jm1; j++) {
            u[i][j] = 0.0;
            uo[i][j] = 0.0;
        }
    }
    // BC
    for(j=0; j<=jm1; j++) {
        u[0][j] = 1.0;
        u[im1][j] = 1.0;
    }
    for(i=0; i<=im1; i++) {
        u[i][0] = 1.0;
        u[i][jm1] = 2.0;
    }
    do {
        gettimeofday(&tv_start, NULL); // timecheck();
        // store old data
        //memcpy(uo, u, sizeof(double) * im1 * jm1);
        for (i=0; i<=im1; i++)
            for (j=0; j<=jm1; j++)
                uo[i][j] = u[i][j] ;
        // jacobi
        for(i=1; i<=im; i++)
            for(j=1; j<=jm; j++)
                u[i][j] = (u[i-1][j] + uo[i+1][j] + u[i][j-1] + uo[i][j+1]) / 4.;
        // error
        error = 0.0;
        for(i=1; i<=im; i++)
            for(j=1; j<=jm; j++)
                error += ( (u[i][j] - uo[i][j]) * (u[i][j] - uo[i][j]) );
        gettimeofday(&tv_end, NULL); // timecheck();
        printf("%d %.16e\n", iter, error);
        // elapsed_time = elapsed_time + time_check(1)-time_check(2);
    } while( (iter++ < ITER_MAX) && (error > tolerance) );
    timeval_subtract(&tv_diff, &tv_end, &tv_start);
    printf("Elapsed time : %ld.%06ld ms.\n", tv_diff.tv_sec, tv_diff.tv_usec);
    print FLOPS between time check routine
    return 0;
}

/* Time function. */
int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1)
{
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;
    return (diff<0);
}