#define N 512
int main()
{
    int i, j, k;
    double a[N][N], b[N][N], c[N][N] ;
    double sum=0.0, flops=0.0;
    struct timeval tv_start, tv_end, tv_diff;
    for (i=0; i<N; i++)
        for(j=0; j<N; j++)
            c[i][j]=0.0;
    printf("O.K");
    gettimeofday(&tv_start, NULL); // timecheck();
    for (i=0; i<N; i++){
        for (j=0; j<N; j++){
            a[i][j] = (double)(i+1)/(double)(j+1);
            b[i][j] = (double)(j+1)/(double)(i+1);
        }
    }
    for (i=0; i<N; i++)
        for (j=0; j<N; j++)
            for (k=0; k<N; k++)
                c[i][j] += a[i][k]*b[k][j];
    for (i=0; i<N; i++)
        for (j=0; j<N; j++)
            sum += sqrt(c[i][j]*c[i][j]);
    gettimeofday(&tv_end, NULL); // timecheck();
    timeval_subtract(&tv_diff, &tv_end, &tv_start);
    printf("Elapsed time : %ld.%06ld ms.\n", tv_diff.tv_sec, tv_diff.tv_usec);
    print FLOPS between time check routine
    return 0;
}
/* Time function. */
int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1)
{
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;
    return (diff<0);
}