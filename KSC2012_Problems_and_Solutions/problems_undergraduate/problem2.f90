program matrix
    implicit none
    integer :: i,j,k,n
    real*8 sum, t1, t2
    real*8,dimension(:,:),allocatable :: a,b,c
    n = 1000
    sum=0.0
    c=0.0
    allocate (a(n,n))
    allocate (b(n,n))
    allocate (c(n,n))
    call cpu_time(t1)
    do j=1,n
        do i=1,n
            a(i,j)=real(i)/real( j)
            b(i,j)=real( j)/real(i)
        end do
    end do
    do k=1,n
        do j=1,n
            do i=1,n
                c(i,j)=c(i,j)+a(i,k)*b(k,j)
            end do
        end do
    end do
    do j=1,n
        do i=1,n
            sum=sum+c(i,j)
        enddo
    enddo
    call cpu_time(t2)
    print *, 'SUM = ',sum
    print *, 'Elapse Time =',t2-t1
    print FLOPS between time check routine
    deallocate(a,b,c)
end program matrix